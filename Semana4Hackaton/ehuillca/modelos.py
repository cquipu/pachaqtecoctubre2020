
class Persona:
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo):
        self.DNI = DNI
        self.Nombre = Nombre
        self.Apellido = Apellido
        self.FechaNacimiento = FechaNacimiento
        self.Sexo = Sexo


class Bibliotecario(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoEmpleado):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoEmpleado = CodigoEmpleado

    def PrestarLibro(self, Libro):
        pass

    def BuscarLibro(self, Libro):
        pass

    def RecibirLibro(self, Libro):
        pass


class Alumno(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoAlumno):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoAlumno = CodigoAlumno

    def PedirPrestado(self, Libro):
        Libro.Cantidad = Libro.Cantidad - 1

    def DevolverLibro(self, Libro):
        Libro.Cantidad = Libro.Cantidad + 1


class Libro:
    def __init__(self, CodigoLibro, Titulo, Autor, Edicion, Categoria, ISBN, Editorial, Cantidad):
        self.CodigoLibro = CodigoLibro
        self.Titulo = Titulo
        self.Autor = Autor
        self.Edicion = Edicion
        self.Categoria = Categoria
        self.ISBN = ISBN
        self.Editorial = Editorial
        self.Cantidad = Cantidad


class Autor(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoAutor):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoAutor = CodigoAutor


class Editorial:
    def __init__(self, CodigoEditorial, RUC, Nombre):
        self.CodigoEditorial = CodigoEditorial
        self.RUC = RUC
        self.Nombre = Nombre
