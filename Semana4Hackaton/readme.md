
  

# Hackatón Semana 4
## **LOGRO**: Programar objetos en **PYTHON**
**“Empecemos a programar!!!”**
Llegó el momento de demostrar lo aprendido durante esta semana, para esto deberás cumplir un reto:
![](poo.png)
**Resuelve el problema en Python mediante POO y súbelo a Gitlab**
### Insumos para resolver el Reto
 - Materiales de clase de la semana 4
### Pasos a seguir para resolver el Reto
 - Descargar la ultima versión del proyecto
 - Crear la carpeta personal en la carpeta Semana4Hackaton
 - Resolver los problemas propuestos y guardarlos en varios archivos diferentes en la carpeta personal
 - El primer archivo será donde el usuario ingrese los datos
 - El segundo archivo contendrá los modelos de las clases del ejercicio
 - Habrán archivos de texto plano con los datos de los objetos
 - Sube tus cambios a tu repositorio git.
 - Envía el **merge request** para integrar los cambios
 
### Solución del Reto
El proyecto debe tener una carpeta personal donde vamos a colocar programación requerida.
El problema a resolver es el siguiente:

La biblioteca de la universidad necesita un sistema de **autoservicio** en el que el alumno y el bibliotecario puedan registrar los libros que se prestan y los libros que se devuelven; por lo tanto, llevar un control de existencias de los libros que se tienen disponibles.
Se debe crear los archivos de texto plano donde estén registrados las siguientes entidades:

	 - Libros
	 - Alumnos
	 - Bibliotecarios

El alumno podrá pedir los libros mediante un buscador y la cantidad de libros de ese titulo restará una unidad de su stock
El alumno podrá devolver los libros mediante el sistema ingresando el ISBN de libro y este aumentará en una unidad el stock de ese titulo

El bibliotecario podrá prestar libros mediante un buscador y la cantidad del libros de ese titulo restará una unidad de su stock
El bibliotecario podrá recibir en devolución los libros mediante el sistema ingresando el ISBN de libro y este aumentará en una unidad el stock de ese titulo

 - El repositorio deberá contar con commits y su push en la rama principal (master)
### Reto cumplido
Para que el reto esté cumplido al 100% deberás tener cubierto los requisitos básicos expuestos:
 - Que se tenga en el gitlab 1 carpeta con el nombre (inicial del nombre y el apellido del estudiante) Dentro de ella, se agregarán los archivos de python y de texto plano los datos de las entidades
 - Revisar que el proyecto (repositorio) tenga un README.md con el nombre del colaborador y del proyecto.
 - Revisar que contengan commits y el push en la rama principal.

