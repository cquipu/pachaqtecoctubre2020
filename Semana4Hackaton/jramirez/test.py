# class Pelicula:
    
#     # Constructor de clase
#     def __init__(self, titulo, duracion, lanzamiento):
#         self.titulo = titulo
#         self.duracion = duracion
#         self.lanzamiento = lanzamiento
#         print('Se ha creado la película:', self.titulo)

#     def __str__(self):
#         return '{} ({})'.format(self.titulo, self.lanzamiento)


# class Catalogo:

#     peliculas = []  # Esta lista contendrá objetos de la clase Pelicula

#     def __init__(self, peliculas=[]):
#         self.peliculas = peliculas

#     def agregar(self, p):  # p será un objeto Pelicula
#         self.peliculas.append(p)

#     def mostrar(self):
#         for p in self.peliculas:
#             print(p)  # Print toma por defecto str(p)

# p = Pelicula("El Padrino", 175, 1972)
# c = Catalogo([p])  # Añado una lista con una película desde el principio
# c.agregar(Pelicula("El Padrino: Parte 2", 202, 1974))  # Añadimos otra
# c.mostrar()
from collections import deque
class Libro:
    def __init__(self, CodigoLibro, Titulo, Autor, Edicion, Categoria, ISBN, Editorial, Cantidad):
        self.CodigoLibro = CodigoLibro
        self.Titulo = Titulo
        self.Autor = Autor
        self.Edicion = Edicion
        self.Categoria = Categoria
        self.ISBN = ISBN
        self.Editorial = Editorial
        self.Cantidad = Cantidad

dic ={"CodigoLibro": "1234", "Titulo": "Calculo Integral", "Autor": "Espinoza Ramos", "Edicion": "3ra", "Categoria": "Matematicas", "ISBN": "888888888", "Editorial": "Lumbreras", "Cantidad": "10"}

for k,v in dic.items():print(v)