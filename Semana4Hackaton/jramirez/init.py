################################ IMPORTS ########################################
import utils
import json
from modelos import Alumno, Bibliotecario, Libro, ColeccionLibros, ColeccionAlumnos, Registrar, Archivos
from utils import Menu
from time import sleep


################################## Lista Opciones Menu ############################################
opciones = {"Alumno": 1, "Biblotecario": 2, "salir":9}
menu = utils.Menu("Menu Principal", opciones)
opcionesAlumno = {"Buscar Libro":1, "Prestar Libro":2,"Devolver Libro":3, "Listar Libros":4,"Listar Alumnos":5, "Registro de alumno":6,"retornar Menu Anterior":8, "salir":9}
opcionesBiblotecario = {"Buscar Libro":1, "Prestar Libro":2, "Devolver Libro":3, "Listar inventario":4, "Registrar Libro":5, "Registrar Alumnos":6, "Listar Alumnos":7,"retornar Menu Anterior":8, "salir":9}
###################################################################################################

#Carga inicial:
archivosLibros = Archivos("Libros.txt")         #Clase Archivo en modelos.py
lstLibrosDic = archivosLibros.CargaInicial()
catalogoLibreria = ColeccionLibros(lstLibrosDic)

archivosAlumnos = Archivos("Alumnos.txt")
lstAlumnosDic = archivosAlumnos.CargaInicial()
catalogoAlumnos = ColeccionAlumnos(lstAlumnosDic)

archivosBiblotecarios = Archivos("Biblotecarios.txt")
lstBibliotecariosDic = archivosBiblotecarios.CargaInicial()
##############################################################################

flag = True
opcion = menu.MostrarMenu()
print(opcion)
registrar = Registrar()

while flag:
    

    if(opcion == 1): # Si es el Usuario Alumno
        
        MenuAlumno = utils.Menu("Menu Alumno", opcionesAlumno)  # Cargar Menu Alumno
        respuesta = MenuAlumno.MostrarMenu()
        if(respuesta == 1): # Si la opcion es: Buscarlibros()
            
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Buscar Libro :::::::::::::\n"+utils.Color('off'))
            print("Digita el titulo del libro a buscar : ")            
            strTituloLibro = input()
            #strTituloLibro = strTituloLibro.strip()
            if(catalogoLibreria.ValidarExistencia('Titulo',strTituloLibro)):
                indexEncontrado = catalogoLibreria.BuscarEnListaDicionario('Titulo',strTituloLibro)                  #Def BuscarEnListaDicionario(self, clave, valorBuscado):
                objlist = catalogoLibreria.RetonarListaObjetos()
                print("Libro Encontrado :")
                print(objlist[indexEncontrado])
            else:
                print(utils.Color('red') + "libro no encontrado \n " + "retornando a menu principal .. "+utils.Color('off'))
            #catalogoLibreria.MostrarObjetos()
            sleep(7)
        elif(respuesta == 3):  # Si la opcion es: DevolverLibro()
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Devolver Libro :::::::::::::\n"+utils.Color('off'))
            print("Ingrese el ISBN del Libro a devolver: ")
            isbnLibroStr = input()
            print(isbnLibroStr)
            if(catalogoLibreria.devolverLibro(isbnLibroStr) == True):
                archivosLibros.Guardar(catalogoLibreria.listaDeDiccionario)
                print("Libro devuelto")                
            else:
                print(utils.Color('red') + "libro no encontrado \n " + "retornando a menu principal .. "+utils.Color('off'))
            sleep(7)
        elif(respuesta == 4):  # Si la opcion es: ListarLibros()            
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Listar Libros :::::::::::::\n"+utils.Color('off'))
            catalogoLibreria.MostrarObjetos()
            sleep(7)
        elif(respuesta == 5):  # Si la opcion es: ListarAlumnos()            
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Listar Alumnos :::::::::::::\n"+utils.Color('off'))
            catalogoAlumnos.MostrarObjetos()
            sleep(7)
        elif(respuesta == 6):  # Si la opcion es: RegistrarAlumno() 
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Registrar Alumno :::::::::::::\n"+utils.Color('off'))
            objAlumnoCreado=registrar.Alumno() # Retorna un objeto Libro que se ingresa por teclado
            print("Se ha registrado a: ",objAlumnoCreado)
            catalogoAlumnos.AgregarObj(objAlumnoCreado)
            catalogoAlumnos.AgregarDic(objAlumnoCreado.dictAlumno())
            archivosAlumnos.Guardar(catalogoAlumnos.RetonarListaDicionario())
            sleep(7)
        elif(respuesta == 8):  # Retorna menu
            opcion = menu.MostrarMenu()
            flag = True     

            #pass
        elif(respuesta == 9):  # Si la opcion es:  Salir

            flag = False   
        else:
            
            print("No hay opcion")
        
    if(opcion == 2): # Si es el Usuario Biblotecario
        
        MenuBiblotecario = utils.Menu("Menu Biblotecario", opcionesBiblotecario)  # Cargar Menu Biblotecario
        respuesta = MenuBiblotecario.MostrarMenu()
        if(respuesta == 1): # Si la opcion es: Buscarlibros()
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Buscar Libro :::::::::::::\n"+utils.Color('off'))
            print("Digita el titulo del libro a buscar : ")            
            strTituloLibro = input()
            #strTituloLibro = strTituloLibro.strip()
            if(catalogoLibreria.ValidarExistencia('Titulo',strTituloLibro)):
                indexEncontrado = catalogoLibreria.BuscarEnListaDicionario('Titulo',strTituloLibro)                  #Def BuscarEnListaDicionario(self, clave, valorBuscado):
                objlist = catalogoLibreria.RetonarListaObjetos()
                print("Libro Encontrado :")
                print(objlist[indexEncontrado])
            sleep(7)
        elif(respuesta == 2):  # Si la opcion es: PrestaLibro()
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Presta Libro :::::::::::::\n"+utils.Color('off'))
            print("Ingrese el Titulo del Libro a prestar: ")
            tituloLibroStr2 = input()
            
            if(catalogoLibreria.prestarLibro(tituloLibroStr2) == True):
                archivosLibros.Guardar(catalogoLibreria.listaDeDiccionario)
                print("Libro Prestado")                
            else:
                print(utils.Color('red') + "libro no encontrado \n " + "retornando a menu principal .. "+utils.Color('off'))
            sleep(7)
        elif(respuesta == 3):  # Si la opcion es: DevolverLibro()
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Devolver Libro :::::::::::::\n"+utils.Color('off'))
            print("Ingrese el ISBN del Libro a devolver: ")
            isbnLibroStr = input()
            print(isbnLibroStr)
            if(catalogoLibreria.devolverLibro(isbnLibroStr) == True):
                archivosLibros.Guardar(catalogoLibreria.listaDeDiccionario)
                print("Libro devuelto")                
            else:
                print(utils.Color('red') + "libro no encontrado \n " + "retornando a menu principal .. "+utils.Color('off'))
            sleep(7)
        elif(respuesta == 4):  # Si la opcion es: ListarInventario()
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Listar Inventario de Libros :::::::::::::\n"+utils.Color('off'))
            catalogoLibreria.MostrarObjetos()
            sleep(7)
        elif(respuesta == 5):  # Si la opcion es: RegistrarLibro()
            
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Registrar Libro :::::::::::::\n"+utils.Color('off'))
            objLibroCreado=registrar.Libro() # Retorna un objeto Libro que se ingresa por teclado
            print("Se ha registrado a: ",objLibroCreado)
            catalogoLibreria.AgregarObj(objLibroCreado)
            catalogoLibreria.AgregarDic(objLibroCreado.dicLibro())
            archivosLibros.Guardar(catalogoLibreria.RetonarListaDicionario())
            sleep(7)
        elif(respuesta == 6):  # Si la opcion es: RegistrarAlumno() 
            
            print(utils.Color('darkmagenta')+"\n\t::::::::::::: Registrar Alumno :::::::::::::\n"+utils.Color('off'))
            objAlumnoCreado=registrar.Alumno() # Retorna un objeto Libro que se ingresa por teclado
            print("Se ha registrado a: ",objAlumnoCreado)
            catalogoAlumnos.AgregarObj(objAlumnoCreado)
            catalogoAlumnos.AgregarDic(objAlumnoCreado.dictAlumno())
            archivosAlumnos.Guardar(catalogoAlumnos.RetonarListaDicionario())
            sleep(7)
        elif(respuesta == 7):  # Si la opcion es: ListarAlumno() 
            print(utils.Color('darkmagenta')+"\n\t:::::::::::::  Listar Alumnos :::::::::::::\n"+utils.Color('off'))
            catalogoAlumnos.MostrarObjetos()
            sleep(7)
           
        elif(respuesta == 8):  # Retorna menu
            opcion = menu.MostrarMenu()
            flag = True    
            pass
        elif(respuesta == 9):  # Si la opcion es:  Salir
            
            flag = False
        else:
            print("No hay opcion")
    if(opcion == 9): # Si es el Usuario Biblotecario
        flag = False
