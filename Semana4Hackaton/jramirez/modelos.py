import os
import utils
import json
class Persona:
    def __init__(self, DNI, Nombre, Apellido, edad, Sexo):
        self.DNI = DNI
        self.Nombre = Nombre
        self.Apellido = Apellido
        self.edad = edad
        self.Sexo = Sexo


class Bibliotecario(Persona):
    def __init__(self, DNI, Nombre, Apellido, edad, Sexo, CodigoEmpleado):
        super().__init__(DNI, Nombre, Apellido, edad, Sexo)
        self.CodigoEmpleado = CodigoEmpleado
    
    def dicBiblotecario(self):
        d = {
            'DNI' : self.DNI,
            'Nombre' : self.Nombre,
            'Apellido' : self.Apellido,
            'edad' : self.edad,
            'Sexo' : self.Sexo,
            'CodigoEmpleado' : self.CodigoEmpleado
        }
        return d

    def PrestarLibro(self, Libro):
        pass

    def BuscarLibro(self, Libro):
        pass

    def RecibirLibro(self, Libro):
        pass


class Alumno(Persona):
    def __init__(self, DNI, Nombre, Apellido, edad, Sexo, CodigoAlumno):
        super().__init__(DNI, Nombre, Apellido, edad, Sexo)
        self.CodigoAlumno = CodigoAlumno
    
    def dictAlumno(self):
        d = {
            'DNI' : self.DNI,
            'Nombre' : self.Nombre,
            'Apellido' : self.Apellido,
            'edad' : self.edad,
            'Sexo' : self.Sexo,
            'CodigoAlumno' : self.CodigoAlumno
        }
        return d
    def __str__(self):
        return '{}\t|{}\t|{}\t|{}\t|{}\t|{}'.format(self.DNI, self.Nombre, self.Apellido, self.edad, self.Sexo, self.CodigoAlumno)

class Libro:
    def __init__(self, CodigoLibro, Titulo, Autor, Edicion, Categoria, ISBN, Editorial, Cantidad):
        self.CodigoLibro = CodigoLibro
        self.Titulo = Titulo
        self.Autor = Autor
        self.Edicion = Edicion
        self.Categoria = Categoria
        self.ISBN = ISBN
        self.Editorial = Editorial
        self.Cantidad = Cantidad

    def dicLibro(self):
        d = { 
            'CodigoLibro' : self.CodigoLibro,
            'Titulo' : self.Titulo,
            'Autor' : self.Autor,
            'Edicion' : self.Edicion,
            'Categoria' : self.Categoria,
            'ISBN' : self.ISBN,
            'Editorial' : self.Editorial,
            'Cantidad' : self.Cantidad
             }
        return d
    def Prestar(self):
        try:
            if (self.Cantidad > 0):
                self.Cantidad = self.Cantidad -1
                return True
            else:
                return False
        except Exception as error:            
            return False
    
    def Devolver(self):
        try:
            self.Cantidad = self.Cantidad + 1
            return True
        except Exception as error:            
            return False
   
    def __str__(self):
        return '{}\t|{}\t|{}\t|{}\t|{}\t|{}\t|{}\t|{}'.format(self.CodigoLibro, self.Titulo, self.Autor, self.Edicion, self.Categoria, self.ISBN, self.Editorial, str(self.Cantidad))


class Listas:
    listaDesobj = []  # Esta lista contendrá objetos de la clase Libro
    listaDeDiccionario = [] # es una lista que contiene dicionarios

    def __init__(self, listaDeDiccionario=[]):
        self.listaDeDiccionario = listaDeDiccionario
        self.listaDesobj = []

    def AgregarObj(self, p):  # p será un objeto Libro
        self.listaDesobj.append(p)

    def AgregarDic(self, p):  # p será un dicionario Libro
        self.listaDeDiccionario.append(p)
    def SetListDic(self,indice, clave, valor):
        self.listaDeDiccionario[indice][clave] = valor
    def getListDic(self,indice, clave):
        return self.listaDeDiccionario[indice][clave]
    def RetonarListaDicionario(self):
        return self.listaDeDiccionario
    def RetonarListaObjetos(self):
        return self.listaDesobj
    def MostrarObjetos(self):
        for p in self.listaDesobj:
            print(p)  # Print toma por defecto str(p)
   
    def BuscarEnListaDicionario(self, clave, valorBuscado):
        for item in self.listaDeDiccionario:
            if (valorBuscado.upper() == item[clave].upper()):
                return self.listaDeDiccionario.index(item)
        return -1
    def ValidarExistencia(self, clave, valorBuscado):  #Dado una lista de diccionario, se busca el valor de una clave (retorna true o false)
        #print(self.listaDeDiccionario)
        for item in self.listaDeDiccionario:
            #print(item)
            if (valorBuscado.upper() == item[clave].upper()):
                return True
        return False

class ColeccionLibros(Listas):
    def __init__(self, listaDeDiccionario=[]):
        super().__init__(listaDeDiccionario=[])
        self.listaDeDiccionario = listaDeDiccionario
        self.listaDesobj = self.ConvertirAListaDeObjetos()
        
    def ConvertirAListaDeObjetos(self):
        for dicLibrosItem in self.listaDeDiccionario: 
            # CodigoLibro, Titulo, Autor, Edicion, Categoria, ISBN, Editorial, Cantidad             
            objLibro = Libro(
                    dicLibrosItem['CodigoLibro'],
                    dicLibrosItem['Titulo'],
                    dicLibrosItem['Autor'],
                    dicLibrosItem['Edicion'],
                    dicLibrosItem['Categoria'],
                    dicLibrosItem[ 'ISBN'],
                    dicLibrosItem['Editorial'],
                    dicLibrosItem['Cantidad']
                    )                
            self.listaDesobj.append(objLibro)
        return self.listaDesobj

    def ConvertirAListaDeDiccionario(self):
        self.listaDeDiccionario = []
        for objLibrosItem in self.listaDesobj: 
            # CodigoLibro, Titulo, Autor, Edicion, Categoria, ISBN, Editorial, Cantidad             
            dicLibro = objLibrosItem.dicLibro()                        
            self.listaDeDiccionario.append(dicLibro)
        return self.listaDeDiccionario

    def devolverLibro(self, isbn_adevolver):
        if (self.ValidarExistencia("ISBN",isbn_adevolver) == True):
            n = self.BuscarEnListaDicionario("ISBN", isbn_adevolver)
            print(n)
            if (self.listaDesobj[n].Devolver() == True):
                self.ConvertirAListaDeDiccionario()
                return True
        else:
            False
    def prestarLibro(self, Titulo_adevolver):
        if (self.ValidarExistencia("Titulo",Titulo_adevolver) == True):
            n = self.BuscarEnListaDicionario("Titulo", Titulo_adevolver)
            print(n)
            if (self.listaDesobj[n].Prestar() == True):
                self.ConvertirAListaDeDiccionario()
                return True
        else:
            False

class ColeccionAlumnos(Listas):
    def __init__(self, listaDeDiccionario=[]):
        super().__init__(listaDeDiccionario=[])
        self.listaDeDiccionario = listaDeDiccionario
        self.listaDesobj = self.ConvertirAListaDeObjetos()
        
    def ConvertirAListaDeObjetos(self):
        for dicAlumnoItem in self.listaDeDiccionario: 
            # CodigoLibro, Titulo, Autor, Edicion, Categoria, ISBN, Editorial, Cantidad             
            objAlumno = Alumno(                    
                     dicAlumnoItem["DNI"],
                     dicAlumnoItem["Nombre"],                     
                     dicAlumnoItem["Apellido"],
                     dicAlumnoItem["edad"], 
                     dicAlumnoItem["Sexo"],
                     dicAlumnoItem["CodigoAlumno"]
                )               
            self.listaDesobj.append(objAlumno)
        return self.listaDesobj
    
class Registrar:

    def Libro(self):
        print("\n")
        # CodigoLibro, Titulo, Autor, Edicion, Categoria, ISBN, Editorial, Cantidad
        self.codigoLibro = input("Ingrese codigoLibro: ")
        self.titulo = input("Ingrese Titulo: ")
        self.autor = input("Ingrese autor: ")
        self.edicion = input("Ingrese edcion: ")
        self.categoria = input("Ingrese categoria: ")
        self.isbn = input("Ingrese ISBN :")
        self.editorial = input("Ingrese editorial :")
        try:
            self.cantidad = int(input("Ingrese cantidad: "))
        except Exception as er:
            print("Debe ingresar un valor númerico: ")
            print(er)
        return Libro(self.codigoLibro, self.titulo, self.autor, self.edicion, self.categoria, self.isbn, self.editorial, self.cantidad)

    def Alumno(self):
        print("\n")
        # DNI, Nombre, Apellido, edad, Sexo, CodigoAlumno
        self.dni = input("Ingrese DNI: ")
        self.nomb = input("Ingrese Nombre: ")
        self.apellido = input("Ingrese Apellido: ")
        self.edad = input("Ingrese Edad: ")
        self.sexo = input("Ingrese Sexo: ")
        self.codigoalumno = input("Ingrese Codigo de Alumno :")
        return Alumno(self.dni, self.nomb, self.apellido, self.edad, self.sexo, self.codigoalumno)
    def Bibliotecario(self):
        print("\n")
        #DNI, Nombre, Apellido, edad, Sexo, CodigoEmpleado
        self.dni = input("Ingrese DNI: ")
        self.nomb = input("Ingrese Nombre: ")
        self.apellido = input("Ingrese Apellido: ")
        self.edad = input("Ingrese Edad: ")
        self.sexo = input("Ingrese Sexo: ")
        self.codigoEmpleado = input("Ingrese Codigo de Alumno :")
        return Alumno(self.dni, self.nomb, self.apellido, self.edad, self.sexo, self.codigoEmpleado)

class Archivos:
    def __init__(self,nombreArchivoTxt):
        self.nombreArchivoTxt = nombreArchivoTxt
        self.fileTxt = utils.FileManager(self.nombreArchivoTxt)
    def CargaInicial(self):  #Devuelve un objeto de la clase Coleccion
        try:
           
            __jsonColecion = self.fileTxt.leerArchivo()
            self.listaDeDicionarios = json.loads(__jsonColecion)
            return self.listaDeDicionarios
        except FileNotFoundError as error:
            print("Archivo no encontrado",error)
            self.fileTxt.escribirArchivo("")
        except Exception as error:
            print("Ha ocurrido un errorr: ",error)
    def Guardar(self,dicAguardar):
        try:
            jsonStr = json.dumps(dicAguardar)
            print("\n Se escrbio en el archivo en la ruta:  ", os.getcwd()+"\\"+self.nombreArchivoTxt)
            self.fileTxt.borrarArchivo()
            self.fileTxt.escribirArchivo(jsonStr)

        except FileNotFoundError as error:
            print("Archivo no encontrado",error)
            self.fileTxt.escribirArchivo("")
        except Exception as error:
            print("Ha ocurrido un errorr: ",error)


        

class Autor(Persona):
    def __init__(self, DNI, Nombre, Apellido, edad, Sexo, CodigoAutor):
        super().__init__(DNI, Nombre, Apellido, edad, Sexo)
        self.CodigoAutor = CodigoAutor


class Editorial:
    def __init__(self, CodigoEditorial, RUC, Nombre):
        self.CodigoEditorial = CodigoEditorial
        self.RUC = RUC
        self.Nombre = Nombre

