/*
El problema a resolver es el siguiente:
- Crearemos un programa donde tengamos en cuenta lo siguiente: 
	 - Alumnos: nombre, identificador, edad, correo 
	 - Salón: nombre, año escolar 
	 - Cursos: nombre, profesor 
	 - Profesores: nombre, identificador, edad, correo 
 - Se deben tener en cuenta las siguiente pautas: 
	 - Se tiene que cada año escolar puede tener bimestre, de las cuales por salón se tienen a profesores asignados a cursos específicos 
	 - Alumno y profesor están asociados a un salón 
	 - El alumno tiene un registro de notas bimestrales (no de exámenes o pruebas) 
	 - El profesor puede cambiarse de curso en un salón específico

 - El repositorio deberá contar con commit's y su push en la rama principal (master)
*/

# Creamos la Base de Datos
DROP DATABASE IF EXISTS emontenegro;
CREATE DATABASE emontenegro CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE emontenegro;

# Creamos las Tablas
DROP TABLE IF EXISTS Salon;
CREATE TABLE Salon
(
  IdSalon        BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre         VARCHAR(50) NOT NULL,

  CONSTRAINT pk_Salon PRIMARY KEY (IdSalon),
  CONSTRAINT uc_Salon UNIQUE (nombre),
  INDEX ix_Salon (nombre)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS Alumno;
CREATE TABLE Alumno
(
  IdAlumno        BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  IdSalon		  BIGINT(20) UNSIGNED,
  nombre          VARCHAR(50) NOT NULL,
  edad            INT(3) NOT NULL,
  correo          VARCHAR(100),

  CONSTRAINT pk_Alumno PRIMARY KEY (IdAlumno),
  CONSTRAINT fk_Alumno_Salon FOREIGN KEY (IdSalon) REFERENCES Salon (IdSalon),
  CONSTRAINT uc_Alumno UNIQUE (correo),
  INDEX ix_Alumno (nombre, correo)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS AnioEscolar;
CREATE TABLE AnioEscolar
(
  IdAnioEscolar	BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  descripcion   VARCHAR(20) NOT NULL,

  CONSTRAINT pk_AnioEscolar PRIMARY KEY (IdAnioEscolar),
  CONSTRAINT uc_AnioEscolar UNIQUE (descripcion),
  INDEX ix_AnioEscolar (descripcion)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS Bimestre;
CREATE TABLE Bimestre
(
  IdBimestre	BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  descripcion   VARCHAR(20) NOT NULL,

  CONSTRAINT pk_Bimestre PRIMARY KEY (IdBimestre),
  CONSTRAINT uc_Bimestre UNIQUE (descripcion),
  INDEX ix_Bimestre (descripcion)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS Profesor;
CREATE TABLE Profesor
(
  IdProfesor	BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  IdSalon		BIGINT(20) UNSIGNED,
  nombre        VARCHAR(50) NOT NULL,
  edad          INT(3) NOT NULL,
  correo        VARCHAR(100),

  CONSTRAINT pk_Profesor PRIMARY KEY (IdProfesor),
  CONSTRAINT fk_Profesor_Salon FOREIGN KEY (IdSalon) REFERENCES Salon (IdSalon),
  CONSTRAINT uc_Profesor UNIQUE (nombre, correo),
  INDEX ix_Profesor (nombre, correo)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS Curso;
CREATE TABLE Curso
(
  IdCurso        BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre         VARCHAR(50) NOT NULL,

  CONSTRAINT pk_Curso PRIMARY KEY (IdCurso),
  CONSTRAINT uc_Curso UNIQUE (nombre),
  INDEX ix_Curso (nombre)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS ProfesorCurso;
CREATE TABLE ProfesorCurso
(
  IdProfesor	BIGINT(20) UNSIGNED NOT NULL,
  IdCurso		BIGINT(20) UNSIGNED NOT NULL,
  
  CONSTRAINT pk_ProfesorCurso PRIMARY KEY (IdProfesor, IdCurso)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS SalonProfesorCurso;
CREATE TABLE SalonProfesorCurso
(
  IdSalon		BIGINT(20) UNSIGNED NOT NULL,
  IdProfesor	BIGINT(20) UNSIGNED NOT NULL,
  IdCurso		BIGINT(20) UNSIGNED NOT NULL,
  
  CONSTRAINT pk_SalonProfesorCurso PRIMARY KEY (IdSalon, IdProfesor, IdCurso)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS AlumnoAnioEscolarBimetre;
CREATE TABLE AlumnoAnioEscolarBimetre
(
  IdAlumno		BIGINT(20) UNSIGNED NOT NULL,
  IdAnioEscolar	BIGINT(20) UNSIGNED NOT NULL,
  IdBimestre	BIGINT(20) UNSIGNED NOT NULL,
  nota   		INT(2) NOT NULL,
  
  CONSTRAINT pk_AlumnoProfesorSalon PRIMARY KEY (IdAlumno, IdAnioEscolar, IdBimestre)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;