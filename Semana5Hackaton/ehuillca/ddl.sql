
CREATE TABLE Alumno (
  idAlumnos Serial NOT NULL,
  nombres VARCHAR(45) NULL,
  apellido_materno VARCHAR(45) NULL,
  apellido_paterno VARCHAR(45) NULL,
  dni VARCHAR(8) NULL,
  edad INT NULL,
  correo VARCHAR(45) NULL,
  PRIMARY KEY (idAlumnos))