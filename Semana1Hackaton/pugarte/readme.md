# Primer proyecto realizado por Paulo Ugarte para primera Hackaton
# `BackEnd`
![](https://descubrecomunicacion.com/wp-content/uploads/2019/07/forndend-backend-post2.jpg)
## `Un desarrollador FrontEnd`

Un desarrollador FrontEnd es aquel que se encarga de programar la parte visual de un software que interactúa directamente con los clientes o usuarios.

-------------
## `Tecnologías que un desarrollador FrontEnd debe conocer:`

Las principales tecnologías básicas que un desarrollador FrontEnd debería conocer deberían de ser HTML, CSS y JAVASCRIPT. No obstante, un desarrollador FrontEnd puede conocer aún más como por ejemplo:

![](https://i.ibb.co/Npmysyn/frontend.png)
---
## `Actividades de un frontEnd`

- Mantener una interacción con el diseñador UX
- Convertir plantillas de páginas a páginas web usando códigos HTML
- Crear una página web que sea adaptable a cualquier dispositivo
---

## `Empresas que aportan al desarrollo frontEnd:`

Sin duda Facebook y Google son empresas que han apostado por el desarrollo FrontEnd, ambas empresas han ofrecido herramientas para facilitar su desarrollo, Facebook aportando con el Framework React y Google por su lado con Angular y Materialize

![](https://www.inmoblog.com/wp-content/uploads/2020/02/ads-google-facebook.jpg)

---
## `¿Cuál es el rol de un Front-end en las empresas del Perú?`

El FrontEnd en Perú se puede apreciar en diferentes empresas bancarias como por ejemplo, BCP, o en diferentes plataformas de compra online como Linio. En pocas palabras el trabajo de un desarrollador FrontEnd es dar una facilidad visualmente al usuario para que dentro de cada paágina pueda hacer lo que realmente está buscando hacer. Que al usuario no le parezca complicado en hecho de usar una interfaz.

![](https://is4-ssl.mzstatic.com/image/thumb/PurpleSource124/v4/16/4e/7e/164e7ebf-1f6d-68e9-bc70-67e19fc5f6eb/455a8acd-fd7d-4a7f-a572-4d31f4160e44_iPhone5.5_02.png/392x696bb.png)

---
## `¿Cuál es el rol de un Front-end en las empresas del mundo?`

Similar al rol que cumplen en el Perú, al final se supone que todos los FrontEnd tienen que lograr una facilidad visual al usuario al momento de interactual con alguna página web. Ejemplos como facebook, Instagram, etc. El FrontEnd tendría que programar una interfaz cómoda para cada tipo de Página, Instagram por ejemplo, es una página que se entendería como una galería de fotos. Facebook por su lado, es una red social en la que puedes compartir contenido, chatear, etc. Un frontEnd hace parecer fácil realizar todas esas tareas mencionadas realizando una cómoda interfaz.

![](https://tynmedia.com/tynmag/wp-content/uploads/sites/3/2020/09/Facebook-Business-Suite.png)
---
## `Analicemos: ¿Qué es lo que más destaca en las siguientes páginas web?`
| Youtube | Interbank |
| ------------ | ------------- |
| ![](https://www.tarrenproduction.co.uk/wp-content/uploads/2016/06/YouTube-logo-200x200.png) | ![](https://2.bp.blogspot.com/--wx3HvIPbcc/Vj-sdXptJNI/AAAAAAAABgo/lMP3yAYS1sA/s1600/008%2BInterbank.jpg) |
## Conclusiones
Ambas páginas son lo suficiente interactivas, Interbank pr su lado muestra una calidad interfaz con botones desplegables que le hacen fácil el entendimiento al usuario, y youtube no tiene necesidad de ser recargada varias veces, pues en la interfaz principal ya te muestra diferentes tipos de videos para que el usuario no tenga necesidad de recargar muchas veces la página.

---
## `Tendencias visuales en el diseño web 2020`
1. #elementosgrandes
2. #primeromóvil
3. #asimétricas

---
## `Roadmap de un desarrollador Front-end`
![](https://miro.medium.com/max/2000/1*FkXWjJN3vdujl9K74GBc8g.jpeg)

---
## `Tendencias para Front-end 2020:`
- Crecimiento de las Progressive Web Apps (PWA) Referencia: Visitar (Comunidad) ...
- Componentes en la Nube. Referencia: Visitar (Herramienta Bit) ...
- Muchos quieren Aprender TypeScript. Referencia: Visitar (Web) ...
- Módulos ECMA Script vía CDN. ...
- Web Assembly (Wasm)

---
## `Un desarrollador Back-end`
El desarrollador BackEnd, a diferencia del FrontEnd es aquel que se encarga de establecer la comunicación con el servidor, es que le da funcionalidad a una página hecha solo por el ado del FrontEnd, Por ejemplo: Es aquel que hace que el ahcer click a un botón logre hacer una acción.
![](https://lh6.googleusercontent.com/qkok_-tZxH64DWVj7O3QqGLiI06-GGdw4VTQV_ulfmwU7o8c2n4UNBRXO7WMD-kXq-HWbvNhhwTG4d5PRdgJk3Rwgc2NxBNoVlfGQuwaBG06TSHF2b7MnJNmqZjSosQQPQG8J-bh)

---
## `Tecnologías de desarrollo que un Back-End debe conocer:`
El rol de BackEnd involucra conocer muchas cosas como el manejo de bases de datos, saber sobre los servidores web, tener conocimientos sobre diferentes lenguajes de programación. Algunos de los tantos lenguajes de programación son los siguientes:
![](https://geekytheory.com/wp-content/uploads/2015/10/programming-language-min.jpg)

---
## `Lenguajes de programación y Frameworks`

- Python: fácil de aprender, veloz, semi-compilado y multipropósito.
El framework para web más usado es Django.
- PHP: por ejemplo, el famoso gestor de contenidos WordPress usa por detrás PHP.
Laravel es uno de los frameworks usados con este lenguaje.
- Ruby: es un lenguaje parec- ido al de Python, su framework más popular es Ruby on
rails.
Node.js: se está haciendo cada vez más popular debido a que usa el mismo lenguaje
que en el lado cliente: JavaScript, su framework para web más usado es Express.
- Java: el lenguaje clásico y uno de los más demandados, su framework Spring.
ASP.NET: es la plataforma de desarrollo web de Microsoft.
---
## `Bases de datos`
- MySQL: base de datos relacional muy usada por
millones de plataformas web actualmente es soportada
por Oracle.
- Postgres: base de datos relacional muy usada y de alto
rendimiento y soporta base de datos geo espacial.
- MongoDB: base de datos no relacional muy usada y
conocida para registrar grandes cantidades de datos a
un costo de tiempo y rendimiento menor.
![](https://revistadigital.inesem.es/informatica-y-tics/files/2016/01/portada.jpg)

---
## `Servidores web y protocolos`
- Nginx: es uno de los servidor web / proxy inverso ligero y de alto
rendimiento y proxy para protocolos de correo como IMAP y POP3.
- Gunicorn: es un servidor http que puede soportar desarrollos web
en python con frameworks como django, flask entre otros.
- HTTPS es un protocolo de aplicación basado en el protocolo HTTP,
destinado a la transferencia segura de datos de Hipertexto, es decir,
es la versión segura de HTTP.

---
## `Actividades de un Back-end`
![](https://i.pinimg.com/originals/11/a4/14/11a414e5fa0bff7d1c519dda7c37e6b3.png)
---
## `Conceptos que debe conocer un desarrollador Back-end`
- Performance: Significa tener una respuesta rápida a sus servicios y que soporte una amplia cantidad de peticiones. El desarrollador Back-end puede abordar estos problemas con
diferentes soluciones, como Memcache, desarrollo transaccional,
desarrollar colas de mensajes, entre otras opciones.
- Infraestructura: un desarrollador Back-end debe entender
algunos conceptos de linux y poder usar un servidor en dicho
sistema operativo, puesto que los grandes servidores del
mundo usan Linux.
Tener clara la infraestructura que está montado el código del
Back-end es una tarea también del desarrollador Back-end
puesto que es importante conocer cuál es el rendimiento de
la aplicación con el código hecho.
- Automatización: Poder realizar procesos automaticamente

---
## `Roadmap de un desarrollador Back-end`
![](https://miro.medium.com/max/1600/1*PAIHikWAcgrDbg1b7Gu7qw.jpeg)
---
## `Tendencias para Back-end 2020:`
![](https://www.teclab.edu.ar/wp-content/uploads/2020/02/tabla.png)

---
## `Analicemos: ¿Dónde crees que pueda estar el backend en las siguientes paginas?`
| Youtube | Interbank |
| ------------ | ------------- |
| ![](https://www.tarrenproduction.co.uk/wp-content/uploads/2016/06/YouTube-logo-200x200.png) | ![](https://2.bp.blogspot.com/--wx3HvIPbcc/Vj-sdXptJNI/AAAAAAAABgo/lMP3yAYS1sA/s1600/008%2BInterbank.jpg) |
Youtube: Al momento de poder observar los videos populares, mas vistos o también los que se acomodan a nuestra búsqueda diaria.|
Interbank: Al momento de poder visualizar las promociones o también dentro de su banca segura.
---
## `Conclusiones`
- Podemos deducir que las tecnologías se van actualizando cada año, creciendo así el
impacto dentro de nuestro mercado laboral.
- Tanto el back-end como el front-end son indispensable para proyectos de desarrollo web.

---