# BACK END
## Un desarrollador Front-end
Un desarrollador front-end es responsable del desarrollo  de la interfaz gráfica que se va interactuar con el usuario tomando consideraciones de experiencia de usuario.
![](FrontendYBackend.png)
### Tecnologías de desarrollo que un Front-End debe conocer
Las tecnologia que debe conocer un Front- End son los siguientes:
HTML/CSS/Angular/JavaScript/Adobe XD/Bootstrap/Reactjs / jQwery / SASS / wordpress
![](frontendTech.png)

### Actividades de un Front-end
Las actividades son los siguientes
 1. Se reune con el equipo de UX para los requerimientos del proyecto a ejecutar 
 2.Convierte los requerimientos en una maquetacion web
 3.Asegurar que el diseno sea adaptable a todas los dispositivos como laptops, tablet, smartphones,iphones etc.
 4.Tambien se preocupa en el diseño para posisionar la pagina en los buscadores (SEO)
### Empresas que aportan al desarrollo Front-end
Las empresas principales que aportan al desarrollo del Front-End son las siguientes :
Facebook, Angular
### ¿Cuál es el rol de un Front-end en las empresas del Perú?
Desarrollar una interfaz web amigable y adaptable a multiples dispositivos permitiendo la facil navegacion para el usuario, considerando téncinas de UX y marketing empresarial.
### ¿Cuál es el rol de un Front-end en las empresas del mundo?
Crean ventanas digitales que ponen en exibición a las empresas,hacen que la fluidez de la pagina sea rápida (jQwert, angular), usan las herramientas para que los clientes se puedan comunicar con las empresas desde la interface web usando chact formularios de contacto. 
![](rolesBackend.png)
### Analicemos: ¿Qué es lo que más destaca en las siguientes páginas web?
Usan Plantillas Bootstrap, wordpress, los cuales se caracterizan por ser responsive, visualmente actractivas, simple de facil operatividad del usuario, hay tendencia de colocar todo en una página.
### Conclusiones
El trabajo del frontend es importante para plasmar los requerimientos empresariales y convertirlos en una vitrina digital en el internet.
### Tendencias visuales en el diseño web 2020
colocar imágenes grandes en la cabecera, hacer un pie de pagína vistoso, colores armoniosos, reponsividad, asimetría.
### Roadmap de un desarrollador Front-end
-Aprender a manejar herramientas de diseño gráfico.
-aprende HTML, CSS, Java script.
-Aprender a usar frameworks.
-Aprende a usar Adobe XD o herramientas similares
-Aprender a usar hosting.
-comprar dominios.
![](frontend.png)
### Tendencias para Front-end 2020:
-Usar componentes de la nube, usar frameworks reacjs, Angular.
-Usar aplicaciones web progresivas, el cual es una aplicación el cual se entrega atravez de la web.
## Un desarrollador Back-end
 Backend es la parte que se conecta con la base de datos y el servidor que utiliza dicho sitio web, por eso decimos que el backend corre del lado del servidor.
 Un desarrollador backend construye toda la parte lógica para el desarrolo de aplicaciones web según las requerimientos del negocio.
 ![](FrontendVsBackend.jpg)

### Tecnologías de desarrollo que un Back-End debe conocer
lenguajes de programacion(java,python,javascript,PHP), frameworks(django,node js), base de datos(mysql,oracle, mongo,postgreSQL),Servidores web,Cloud Computing
### Lenguajes de programación y Frameworks 
Python(Django),PHP(Laravel),Ruby,Nodejs,Java(Spring),ASP.NET,express,
### Bases de datos
base de datos(mysql,oracle, mongo,postgreSQL
### Servidores web y protocolos
Apache,Nginx,Gunicorn,HTTPS
### Actividades de un Back-end
1. Analizar y diseñar modelos de base datos
2. Coordinar con el desarrollador Frontend la comunicación de los servicios.
3. Desarrollar código que cumpla las condiciones del negocio 
4. Seguridad y despliegue de código fuente 

### Conceptos que debe conocer un desarrollador Back-end
Base de Datos,Cloud,Linux,Servidores de Correo
### Roadmap de un desarrollador Back-end
![](backend.png)
### Tendencias para Back-end 2020:
![](lenguajesPopulares.png)
![](FrameworksBakend.png)
### Analicemos: ¿Dónde crees que pueda estar el backend en las siguientes paginas?
BCP, Plaza Vea, Tottus, Incafarma, Lineo, Ebay, Amazon etc
### Conclusiones
Tanto el backend como el frontend se integran para brindar soluciones empresariales .
