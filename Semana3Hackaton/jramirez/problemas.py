
'''
Metodos para resolver los problemas
'''
'''
Escribir un método de un programa que permita 
leer la edad y peso de una persona y posteriormente imprimirla
'''
def problema1(edad, peso):
    return f"Tu edad es {edad} y tu peso es {peso}"
"""
//Escribir un Pseudoc�digo que calcule el area de un triangulo recibiendo como entrada el valor de base y altura
"""
def problema2(base, altura):
   
    area = base*altura/2

    return f"El area del triangulo es : {area}"

"""
	//Escribir un Pseudoc�digo que calcule el �rea de un tri�ngulo recibiendo como entrada el valor de base y altura

"""
def problema3(radio):
    
    PI = 3.1415       
    area = PI * (radio*radio)
    return f"El area del Circulo es : {area}"

#Escribir Pseudocodigo que dados 2 valores de 
# entrada imprima siempre la división del mayor entre el menor.
def problema4(valor1, valor2):        
    if (valor1 > valor2):
       return valor1/valor2
    else:
       return valor2/valor1
   
"""
	//Escribir Pseudocódigo que lea de entrada 3 números y que indique cual es el mayor de ellos.
"""
def problema5(num1, num2, num3):
       
    if (num1>num2 and num1>num3):
        return num1
    
    if (num2>num1 and num2>num3):
        return num2
    
    if (num3>num1 and num3>num2):
        return num3
    """
	//Escribir un Pseudocódigo que lea 3 números los cuales significan una fecha (día, mes, año). Comprobar que sea válida la fecha, si no es valido que imprima un mensaje de error, 
    //y si es válida imprimir el mes con su nombre.
    """
    
def problema6(dia, mes, ano):
           
    if (dia > 0 and dia <= 31) : 
    
        if (mes > 0  and mes <= 12) :            
    
            if (ano >= 1900 and ano <= 2030):
                return f"{dia}/{mes}/{ano}"
                
    else:
        
        print (" debe introducir el dia, mes, anio correcto: ")
    
    """   
    //Escribir un Pseudocodigo que pida la edad y el sexo
    // y dependiendo si es hombre o mujer y si puede votar o no.
    """
def problema7():
    sexo = ""
    edad = 0
    evaluar = True
    while evaluar:
        sexo = str(input("Ingresar sexo (M/F):"))
        if(sexo == "F" or sexo == "M"):            
            evaluar = False
    evaluar = True
    while evaluar:
        edad = int(input("Ingresar edad: "))
        if(edad!=0):
            evaluar = False
    
    if(sexo == "M" and edad >= 18):
        return (" Eres hombre puedes votar")
    if(sexo == "F" and edad >= 18):
        return (" Eres Mujer puedes votar")
    if(sexo == "F" and edad < 18):
        return (" Eres mujer y no puedes votar")
    if(sexo == "M" and edad < 18):
        return (" Eres hombre y no  puedes votar")

# //Realice un programa que calcule la nomina salarial neto, de unos obreros cuyo trabajo se paga en horas. 
# //El c�lculo se realiza de la siguiente forma:
# //-Las primeras 35 horas a una tarifa fija.
# //-Las horas extras se pagan a 1.5 mas de la tarifa fija.
# //-Las horas extras se pagan a 1.5 mas de la tarifa fija.
# //-Los impuestos a deducir de los trabajadores varian, segun el sueldo mensual 
# //	si el sueldo es menos a S./ 20,000.00 el sueldo es libre de impuesto 
# //	y si es al contrario se cobrara un 20% de impuesto.

def problema8():
    h= 0
    sueldoXhora = 0
    pagoNeto = 0
    
    print ("=========== Programa que calcula el Sueldo ==============")
    h = int(input("Ingrese el n�mero de horas trabajadas: "))
    sueldoXhora = float(input("Ingresar El sueldo por Hora: "))
    
    if h<=35:
        pagoNeto = h*sueldoXhora
    else:
        pagoNeto = sueldoXhora*35 + sueldoXhora*1.5*(h-35)
    if (pagoNeto<20000):
        return f"El sueldo es= {pagoNeto}" 
    else:
        return f"Debe Pagar Impuesto, El sueldo es= {pagoNeto*0.8}"

# 9- Escribir un Pseudoc�digo que encuentre y despliegue los n�meros primos entre uno y cien. 
# //Un numero primo es divisible entre el mismo y la unidad por lo tanto un numero primo no puede ser par excepto el dos (2).

def problema9():
    print(" ======== Numeros Primos del 1 al 100 ====")
    primo = 1
    
    while primo<=100:
        divisores =0
        for i in range(1,primo+1):
            if(primo%i == 0):
                divisores=divisores+1
        if divisores <= 2:
            print(primo)
        primo = primo + 1
        
# 10-Hacer un programa que calcule el factorial de un n�mero.
# Proceso Factorial
def problema10(n):
    print("======= Programa que calcula el Facorial ======== \n")
    fact = n
    if (n >= 1):
        if n == 1 or n == 0:            
            return 1
            # Si el numero es igual a 1 su factorial es 1
        else:
            # Sino calcular su factorial
            for i in range(2,n):
                fact=fact*i
            return fact     
    else:
        # Si el numero no es un entero>=1:
        return 1
#11-Mostrar la tablas de multiplicar 
def problema11():
    print("===== Tabla de Multiplicar =======\n")
    for i in range (1,13):
        print(f"======= Tabla de Multiplicar del {i} ======")
        for j in range(1,13):
            print(f"{i} x {j} = {i*j}")
        print("==============================================\n")
#12-que lea números enteros hasta teclear 0, y nos muestre 
# el máximo, el mínimo y la media de todos ellos. 
# Piensa como debemos inicializar las variables.

def problema12():
    valLogic = True
    suma = 0
    registro = []  
    while valLogic:
        try:
            n = int(input("Ingrese un número, si desea terminar ingrese cero: "))
            n = n * 1
            if (n == 0):
                valLogic = False
            else:
                registro.append(n)
                suma = suma + n
        except Exception as error:
            print(f"Ha ocurrido un error: {error}")
    registro.sort()
    print(f"El Maximo es= {registro[len(registro)-1]}")
    print(f"El Minimo es= {registro[0]}")
    print(f"El Promedio es= {suma/len(registro)} \n")
#13-Dada una secuencia de n�meros leidos por teclado, 
# que acabe con un -1, por ejemplo: 5,3,0,2,4,4,0,0,2,3,6,0,��,-1; 
# Realizar un metodo que calcule la media aritm�tica. 
# Suponemos que el usuario no insertara numero negativos.

def problema13():
    valLogic = True
    suma = 0
    registro = []  
    while valLogic:
        try:
            n = int(input("Ingrese un número, si desea terminar ingrese -1: "))
            n = n * 1
            if (n == -1):
                valLogic = False
                break
            if (n >= 0):
                registro.append(n)
                suma = suma + n
                
        except Exception as error:
            print(f"Ha ocurrido un error: {error}")
    registro.sort()
    prom = suma/len(registro)
    print(f"El Promedio es= {prom} \n")
#14-Una tienda ofrece un descuento del 15% sobre el total de la compra durante el mes de octubre.
# Dado un mes y un importe, calcular cu�l es la cantidad que se debe cobrar al cliente.
def problema14(num):
    try:
        mes = str(input("ingrese un mes del anio: "))
        mes.lower()
        if(mes == "octubre"):
            return num*0.85
        else:
            return num
    except Exception as error:
            print(f"Ha ocurrido un error: {error}")
    

#15.- Dados 10 numeros enteros que se ingresan por teclado, calcular cu�ntos de ellos son pares, cu�nto suman ellos y el promedio de los impares

def problema15():
    
        print("=== Programa que pide ingresar 10 numeros y calcula cuantos de ellos son pares, cuanto suman ellos y el promedio de los impares==")
        print("Es necesario ingresar 10numeros \n")
        num = []
        pares = []
        impares = []
        sumPares = 0.0
        sumImpares = 0.0
    
        try:            
            for i in range(1,11):
                try:                
                    num.append(float (input(f"ingrese el valor del numero {i}: ")))
                except Exception as error:
                    return f"Ha ocurrido un error: {error}"
            for k in num:
                if (k%2 == 0):
                    pares.append(k)                    
                    sumPares = sumPares + k
                else:
                    impares.append(k)
                    sumImpares = sumImpares + k
            if len(impares)==0:
                prom = 0
            else:
                prom =  sumImpares/len(impares)   
            return f"""
                    El numero de pares es: {len(pares)}
                    La suma de los pares es: {sumPares}
                    La suma total de todos los numeros es: {sumImpares+sumPares}
                    El promedio de los impares es: {prom}
                    """
        except Exception as error:
            print(f"Ha ocurrido un error: {error}")