'''
Inicio del Programa
'''
import problemas

menu = True

while menu:
    try:
        prob = int(input("Escoja un problema (1 - 15) o digita 0 para salir: "))
        if(prob == 0):
            print("Saliendo del programa")
            menu = False
        if(prob == 1):
            peso = int(input("Digita tu peso en numeros "))
            edad = int(input("Digita tu edad en numeros "))
            respuesta = problemas.problema1(edad, peso)
            print(f"la respuesta es {respuesta}")

    except Exception as error:
        print(f"Ha ocurrido un error: {error}")