##  Inicio del Programa
##
import problemas

menu = True

while menu:
    try:
        prob = int(input("Escoja un problema (1 - 15) o digita 0 para salir: "))
        if(prob == 0):
            print("Saliendo del programa")
            menu = False
        if(prob == 1):
            peso = int(input("Digita tu peso en numeros "))
            edad = int(input("Digita tu edad en numeros "))
            respuesta = problemas.problema1(edad, peso)
            print(f"la respuesta es {respuesta}")
        if(prob == 2):   
            base = int(input("Introduce base de triangulo "))
            altura = int(input("Introduce altura de triangulo ")) 
            respuesta = problemas.problema2(base, altura)
            print(f"El area del triangulo es {respuesta} ")	    
        if(prob == 3):
            radio = int(input("Introduce el valor del radio del circulo "))
            respuesta = problemas.problema3(radio)
            print(f"El area del circulo es {respuesta} ")
        if(prob == 4):    
            try:
                PrimerNumero = int(input("Escriba el Primer Numero ")) 
                SegundoNumero = int(input("Escriba el Segundo Numero "))  
                respuesta = problemas.problema4(PrimerNumero, SegundoNumero)
                print(f"la division del mayor entre el menor numero es {respuesta}")
            except ValueError:
	            print("No se puede escribir en letras los numeros ")
            except ZeroDivisionError:
	            print("No se puede dividir para cero ")
        if(prob == 5):
            try:
                PrimerNumero = int(input("Introduce el primer numero "))
                SegundoNumero = int(input("Introduce el segundo numero "))
                TercerNumero = int(input("Introduce el tercer numero "))
                respuesta = problemas.problema5(PrimerNumero, SegundoNumero, TercerNumero)
                print(f" {respuesta} ")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")
        if(prob == 6):    
            try:
                dia = int(input("introduzca dia "))   
                mes = int(input("introduzca mes "))
                año = int(input("introduzca año "))
                respuesta = problemas.problema6(dia, mes, año)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")
        if(prob == 7):
            try:
                edad = int(input("introduzca su edad "))   
                sexo = input("introduzca sexo: hombre o mujer ")
                respuesta = problemas.problema7(edad, sexo)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")
        if(prob == 8):            
            try:
                horas = int(input("introduzca sus horas trabajadas "))                
                pago = int(input("introduzca su pago por hora "))
                respuesta = problemas.problema8(horas, pago)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")   
        if(prob == 9):
            try:
                print("Encuentre los 100 primeros numeros primos")
                A = int
                respuesta = problemas.problema9(A)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ") 
        if(prob == 10):
            try:
                numero = int(input("introduzca un numero "))            
                respuesta = problemas.problema10(numero)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")
        if(prob == 11): 
            try:
                numero = int(input("introduzca un numero "))
                respuesta = problemas.problema11(numero)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")
        if(prob == 12):
            try:
                numero = int(input("introduzca un numeros hasta teclear 0 "))
                respuesta = problemas.problema12(numero)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")
        if(prob == 13):
            try:
                numero = int(input("introduzca un numeros hasta teclear -1 "))
                respuesta = problemas.problema13(numero)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")
        if(prob == 14):
            try:
                importe = int(input("introduzca importe de compra "))                
                mes = input("introduzca el mes ")
                respuesta = problemas.problema14(importe, mes)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")
    except Exception as error:
        if(prob == 15):
            try:
                numero = int(input("introduzca 10 numeros "))
                respuesta = problemas.problema15(numero)
                print(f" {respuesta}")
            except ValueError:
            	print("No se puede escribir en letras los numeros ")
        print(f"Ha ocurrido un error: {error}")
<<<<<<< HEAD
=======


>>>>>>> develop
