#Libreriras Python
import math

# - Escribir un método de un programa que permita leer la edad y peso de una persona y posteriormente imprimirla
def DevolverDatos(edad, peso):
    if (edad > 0) and (peso > 0):
        return f"Tu edad es {edad} y tu peso es {peso}"    
    return f"Datos no validos, la edad {edad} y el peso {peso} no pueden ser negativos"

# - Escribir un método que calcule el área de un círculo.
PI = 3.141516
def AreaCirculo(radio):
    if radio > 0: 
        return PI * radio * radio
    return "Message: El radio del circulo debe ser un numero positivo"

# - Escribir un método que calcule el área de un triángulo recibiendo como entrada el valor de base y altura
def AreaTriangulo(base, altura):
    if (base > 0) and (altura > 0):
        return (base * altura) / 2
    
    return "Message: La base y la altura del triangulo deben ser un numeros positivos"

# - Escribir un método que dados 2 valores de entrada imprima siempre la división del mayor entre el menor.
def DivisionMayorMenor(valor1, valor2):
    if valor1 < valor2:
        return (valor2 / valor1)
    return  f"La division de {valor1} por {valor2} es ", (valor1 / valor2)

# - Escribir un método que lea de entrada 3 números y que indique cual es el mayor de ellos.
def MayorValor3Numeros(valor1, valor2, valor3):
    mayorValor = valor1
    if(mayorValor < valor2):
        mayorValor = valor2

    if(mayorValor < valor3):
        mayorValor = valor3
    
    return  f"El mayor valor de {valor1}, {valor2}  y {valor3} es: ", mayorValor

# - Escribir un método que lea 3 números los cuales significan una fecha (día, mes, año). Comprobar que sea válida la fecha, si no es valido que imprima un mensaje de error, y si es válida imprimir el mes con su nombre.
def ValidarFecha(dia, mes, anio):
    listaMeses = ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
    dias_mes = [31, 28, 31, 30,31, 30, 31, 31, 30, 31, 30, 31]
    
    #Comprobar si el ano es bisiesto y anadir dia en febrero en caso afirmativo
    if((anio % 4 == 0 and anio % 100 != 0) or anio % 400 == 0):
        dias_mes[1] = dias_mes[1] + 1
    
    #Comprobar que el mes sea valido
    if(mes < 1 or mes > 12):
        return f"La fecha ingresada es incorrecta. ingresaste la fecha {dia} - {mes} - {anio}"
    
    #Comprobar que el dia sea valido
    mes = mes - 1
    if(dia <= 0 or dia > dias_mes[mes]):
        return f"La fecha ingresada es incorrecta. ingresaste la fecha {dia} - {mes} - {anio}"
    print(mes)

    nombreMes = listaMeses[mes]
    return f"La fecha ingresada es valida. Estas el mes {nombreMes} ({mes+1})"

# - Escribir un método que pida la edad y el sexo y dependiendo si es hombre o mujer y si puede votar o no.
def PuedeVotar(edad, sexo):
    if edad > 18:
        return f"Usted es un(a) {sexo} de {edad} años: Puede votar"
    else:
        return f"Usted es un(a) {sexo} de {edad} años: No puede votar"


# - Realice un método que calcule la nómina salarial neto, de unos obreros cuyo trabajo se paga en horas. El cálculo se realiza de la siguiente forma:
# > Condiciones
# Las primeras 35 horas a una tarifa fija.
# Las horas extras se pagan a 1.5 más de la tarifa fija.
# Los impuestos a deducir de los trabajadores varían, según el sueldo mensual si el sueldo es menos a S./ 20,000.00 el sueldo es libre de impuesto y si es al contrario se cobrará un 20% de impuesto.

def NominaSalarioNeto(tarifa, horasDeTrabajo):
    sueldo = 0
    if horasDeTrabajo < 36:
        sueldo = horasDeTrabajo * tarifa
    else:
        sueldo = (35 * tarifa) + ((horasDeTrabajo - 35) * (tarifa + 1.5))
    
    if sueldo < 20000:
        return sueldo
    else:
        return 0.8 * sueldo

# - Escribir un método que encuentre y despliegue los números primos entre uno y cien. Un número primo es divisible entre el mismo y la unidad por lo tanto un numero primo no puede ser par excepto el dos (2).
def EsPrimo(numero):
    rpta = True
    for i in range(2, numero):
        if numero % i == 0: 
            return False
    return rpta

def ObtenerNumerosPrimos(maxNum=100):    
    listPrimos = list()
    listPrimos.append(2)

    for val in range(3, maxNum):
        #n = int(math.sqrt(val)) + 1
        if EsPrimo(val):
            listPrimos.append(val)
        
    for primo in listPrimos:
        print(primo, end=" ")
    print("")
  
# - Hacer un método que calcule el factorial de un número.
def Factorial(numero):
    if numero == 1:
        return 1
    else:
        return numero * Factorial (numero-1)

# - Hacer un método que despliegue las tablas de multiplicar.
def TablaDeMultiplicar():
    for i in range(1, 13):
        print(f"Tabla de multiplicar de {i}")
        for j in range (1,13):
            print(f"{i}*{j} =", (i*j))
        print("")

# - Un método que lea números enteros hasta teclear 0, y nos muestre el máximo, el mínimo y la media de todos ellos. Piensa como debemos inicializar las variables.

def MaximoMinimoMedia():
    maximo = media = 0
    minimo = 100000 # Podemos darle el mayor valor posible que puede tomar SO
    Continuar = True
    listaNumeros = list()
    print("Para dejar de leer los numeros ingresa cero (0)")
    while Continuar:
        numero = int(input("Ingresa un numero: "))
        if numero == 0:
            Continuar = False            
        else:
            if numero < minimo:
                minimo = numero
            if maximo < numero:
                maximo = numero
            listaNumeros.append(numero)

    if (len(listaNumeros) == 0):
        return f"Maximo: {maximo},  Minimo: {minimo}, Media: 0"
    
    media = MediaAritmetica(listaNumeros)
    return f"Maximo: {maximo},  Minimo: {minimo}, Media: {media}"


# - Dada una secuencia de números leídos por teclado, que acabe con un –1, por ejemplo: 5,3,0,2,4,4,0,0,2,3,6,0,……,-1; Realizar un método que calcule la media aritmética. Suponemos que el usuario no insertara numero negativos.
def MediaAritmetica(listaNumeros):
    suma = 0
    for num in listaNumeros:
        suma = suma + num

    return (suma / len(listaNumeros))

def ObtenerMediaAritmeticaLista():
    valor = 0
    bandera = True
    listaNumeros = list()

    print("Ingresa una tu lista de numeros")
    print("Para salir digita -1")

    while bandera:
        try:
            valor = int(input("Ingrese un numero: ")) 
            if valor >= 0:           
                listaNumeros.append(valor)            
            else:
                if (valor == -1):
                    bandera = False
                    continue                                
                print("Ingresaste un numero negativo, no se añadio a la lista")            
            
        except Exception as exep:
            print("Error: No has ingresado un numero" + str(exep))
    if len(listaNumeros) == 0:
        return "No ingresaste valores"
    
    return MediaAritmetica(listaNumeros)



# - Una tienda ofrece un descuento del 15% sobre el total de la compra durante el mes de octubre. Dado un mes y un importe, calcular cuál es la cantidad que se debe cobrar al cliente.

def CostoTotal(mes, importe):
    mesPromo ="octubre"
    if mes.lower() == mesPromo:
        importe = 0.85*importe
    
    return importe


# - Dados 10 números enteros que se ingresan por teclado, calcular cuántos de ellos son pares, cuánto suman ellos y el promedio de los impares
def TotalPares(listaNumeros):
    total = 0
    for num in listaNumeros:
        if num % 2 == 0:
            total = total + 1

    return total

def SumaElementosLista(listaNumeros):
    suma = 0
    for num in listaNumeros:
        suma = suma + num

    return suma

def PromedioNumerosImpares(listaNumeros):
    listaImpares = list()   
    for num in listaNumeros:
        if num % 2 == 1:
            listaImpares.append(num)

    return MediaAritmetica(listaImpares)

def Leer10Numeros_MostrarInformacion():
    contador = 0
    listaNumeros = list()
    while contador < 10:
        valor = int(input(f"Ingresa valor {contador + 1}: "))
        listaNumeros.append(valor)
        contador = contador + 1
    print(f"En la lista tenemos {TotalPares(listaNumeros)} numero(s) par(es)")
    print(f"Suma de elementos de la lista: {SumaElementosLista(listaNumeros)}")
    print(f"Promedio de numeros impares: {PromedioNumerosImpares(listaNumeros)}")
