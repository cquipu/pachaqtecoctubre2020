'''
Metodos para resolver los problemas
'''
'''
Escribir un método de un programa que permita 
leer la edad y peso de una persona y posteriormente imprimirla
'''
def problema1(edad, peso):
    return f"Tu edad es {edad} y tu peso es {peso}"

#//Dados 10 n�meros enteros que se ingresan por teclado, calcular cu�ntos de ellos son pares, 
# cu�nto suman ellos y el promedio de los impares

def problema15():
    
        print("=== Programa que pide ingresar 10 numeros y calcula cuantos de ellos son pares, cuanto suman ellos y el promedio de los impares==")
        print("Es necesario ingresar 10numeros \n")
        num = []
        pares = []
        impares = []
        sumPares = 0.0
        sumImpares = 0.0
    
        try:            
            for i in range(1,11):
                try:                
                    num.append(float (input(f"ingrese el valor del numero {i}: ")))
                except Exception as error:
                    return f"Ha ocurrido un error: {error}"
            for k in num:
                if (k%2 == 0):
                    pares.append(k)                    
                    sumPares = sumPares + k
                else:
                    impares.append(k)
                    sumImpares = sumImpares + k
            if len(impares)==0:
                prom = 0
            else:
                prom =  sumImpares/len(impares)   
            return f"""
                    El numero de pares es: {len(pares)}
                    La suma de los pares es: {sumPares}
                    La suma total de todos los numeros es: {sumImpares+sumPares}
                    El promedio de los impares es: {prom}
                    """
        except Exception as error:
            print(f"Ha ocurrido un error: {error}")


