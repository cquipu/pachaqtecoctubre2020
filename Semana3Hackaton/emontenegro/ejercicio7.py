'''
Escribir un método que pida la edad y el sexo y dependiendo si es hombre o mujer y si puede votar o no.
'''

while True:
    try:
        edad = int(input("Digitar la edad: "))
        sexo = input("Digitar el sexo: ")
        
        puedeVotar = "Si puede Votar" if edad > 18 else "No puede Votar"

        print(f"{puedeVotar}")
        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")


        