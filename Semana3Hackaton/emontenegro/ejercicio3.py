'''
Escribir un método que calcule el área de un círculo.
'''

# Import math Library
import math

while True:
    try:
        radio = float(input("Digitar el radio: "))
        area = math.pi * math.pow(radio, 2)
        print(f"El area del circulo es: {area}")
        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")


        