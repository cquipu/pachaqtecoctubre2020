'''
Realice un método que calcule la nómina salarial neto, de unos obreros cuyo trabajo se paga en horas. El cálculo se realiza de la siguiente forma
Condiciones:
    Las primeras 35 horas a una tarifa fija.
    Las horas extras se pagan a 1.5 más de la tarifa fija.
    Los impuestos a deducir de los trabajadores varían, según el sueldo mensual si el sueldo es menos a S./ 20,000.00 el sueldo es libre de impuesto y si es al contrario se cobrará un 20% de impuesto.
'''

while True:
    try:
        sueldo = float(input("Ingrese Sueldo Mensual: "))
        horas = float(input("Ingrese Horas Trabajadas: "))
        impuesto = 0

        if horas > 35:
            horas = 35 + (horas-35) * 1.5
	
        if sueldo >= 20000:
            impuesto = 0.2
	
        sueldo = (sueldo/30/24) * horas
	
        sueldoTotal = sueldo - (sueldo * impuesto)
	
        print(f"El sueldo del trabajador es: {round(sueldoTotal, 2)}")

        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")