'''
Hacer un método que calcule el factorial de un número.
'''

def factorial (num):
    if num == 1:
        return 1
    else:
        return num * factorial(num - 1)

while True:
    try:
        num = int(input("Ingrese un Numero: "))

        resultado = factorial(num)

        print(f"El factorial del numero es: {resultado}")
        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")