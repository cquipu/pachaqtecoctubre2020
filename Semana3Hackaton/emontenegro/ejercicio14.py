'''
Una tienda ofrece un descuento del 15% sobre el total de la compra durante el mes de octubre. Dado un mes y un importe, calcular cuál es la cantidad que se debe cobrar al cliente.
'''

montoCobrado: float

while True:
    try:
        mes = int(input("Ingrese Mes: "))
        importe = float(input("Ingrese Importe: "))
        
        if mes > 0 and mes < 13:
            if mes == 10:
                montoCobrado = importe * 0.85
            else:
                montoCobrado = importe
            
            print(f"El monto cobrado es: {montoCobrado}")
        else:
            print("Ingrese un Mes Valido")

        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")