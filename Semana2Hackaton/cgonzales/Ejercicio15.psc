Proceso DiezNumeros
	Definir valores, cantidad, pares, suma Como Entero
	Definir  promedioImpar Como Real
	pares = 0
	promedioImpar = 0
	suma = 0
	para cantidad = 1 Hasta 10
		Escribir "Ingrese numero", cantidad
		Leer valores
		si (valores mod 2) = 0
			pares = pares + 1
		SiNo
			promedioImpar = promedioImpar + valores
		FinSi
		suma = suma + valores
	FinPara
	Escribir "Se ingresaron ", pares " numeros pares"
	Escribir "La suma de los numeroes ingresados es: ", suma
	Escribir "El promedio de los numeros impares es: ", promedioImpar/(10-pares)
FinProceso
