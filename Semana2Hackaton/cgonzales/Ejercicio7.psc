Proceso EmitirVoto
	Definir a,b Como Entero
	Escribir "Cuantos a�os tiene usted?"
	Leer a
	Escribir "Indique 1 si es Hombre o 2 si es Mujer"
	Leer b
	si b = 1 O b = 2 Entonces
		si a >= 18
			si b = 1 Entonces
				Escribir "Eres Hombre y puedes votar"
			SiNo
				Escribir "Eres Mujer y puedes votar"
			FinSi
		FinSi
		si a > 0 Y a < 18
			si b = 1 Entonces
				Escribir "Eres Hombre y no puedes votar"
			SiNo
				Escribir "Eres Mujer y no puedes votar"
			FinSi
		FinSi
		si a <= 0
			Escribir "Es una edad incorrecta"
		FinSi
	SiNo
		Escribir "Solo puede indicar 1 o 2, otro valor es incorrecto"
	FinSi
FinProceso
