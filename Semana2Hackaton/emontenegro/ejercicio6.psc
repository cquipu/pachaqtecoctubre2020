SubProceso res <- nombreMes ( mes )
	Segun mes Hacer
		1: res = "Enero"
		2: res = "Febrero"
		3: res = "Marzo"
		4: res = "Abril"
		5: res = "Mayo"
		6: res = "Junio"
		7: res = "Julio"
		8: res = "Agosto"
		9: res = "Setiembre"
		10: res = "Octubre"
		11: res = "Nomviembre"
		De Otro Modo: res = "Diciembre"
	Fin Segun
Fin SubProceso

Proceso ejercicio6
	Escribir "Verificar si una Fecha es valida"
	Escribir "Ingrese Anio:"
	Leer anio
	Escribir "Ingrese Mes:"
	Leer mes
	Escribir "Ingrese Dia:"
	Leer dia
	
	Si anio>=1900 y anio<=9999 Entonces
		Si mes>=1 y mes<=12 Entonces
			Si ((dia >= 1 y dia <= 31) y (mes = 1 o mes = 3 o mes = 5 o mes = 7 o mes = 8 o mes = 10 o mes = 12)) Entonces
				Escribir "La fecha es valida: ",dia,"/",nombreMes(mes),"/",anio
			SiNo
				Si ((dia >= 1 && dia <= 30) && (mes = 4 o mes = 6 o mes = 9 o mes = 11)) Entonces
					Escribir "La fecha es valida: ",dia,"/",nombreMes(mes),"/",anio
				SiNo
					Si ((dia>=1 y dia<=28) y (mes==2)) Entonces
						Escribir "La fecha es valida: ",dia,"/",nombreMes(mes),"/",anio
					SiNo
						Si (dia = 29 && mes = 2 && (anio Mod 400 = 0 ||(anio Mod 4 = 0 && anio Mod 100 <> 0))) Entonces
							Escribir "La fecha es valida: ",dia,"/",nombreMes(mes),"/",anio
						SiNo
							Escribir "El dia no es valido"
						FinSi
					FinSi
				FinSi
			FinSi
		SiNo
			Escribir "El mes no es valido"
		FinSi
	SiNo
		Escribir "El anio no es valido"
	FinSi
FinProceso
