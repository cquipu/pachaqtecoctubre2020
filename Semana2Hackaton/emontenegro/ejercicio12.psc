Proceso ejercicio12
	Definir maximo, minimo, suma Como Entero
	Definir media Como Caracter
	
	i=0
	Repetir
		Escribir "Ingresa un Numero: "
		Leer num
		
		Si num <> 0 Entonces
			//Asignacion del Mayor
			Si (maximo = 0) Entonces
				maximo = num
			SiNo
				Si (num > maximo) Entonces
					maximo = num;
				FinSi
			FinSi
			
			//Asignacion del Menor
			Si (minimo = 0) Entonces
				minimo = num
			SiNo
				Si (num < minimo) Entonces
					minimo = num;
				FinSi
			FinSi
			
			//Asignacion del Promedio
			suma = suma + num
			i = i + 1
		FinSi
	Hasta Que num = 0
	
	//Analizando Division entre 0
	Si i = 0 Entonces
		media = "ND"
	SiNo
		media = ConvertirATexto(suma/i)
	FinSi
	
	Escribir "El numero maximo es: ", maximo
	Escribir "El numero minimo es: ", minimo
	Escribir "La media es: ", media
FinProceso
