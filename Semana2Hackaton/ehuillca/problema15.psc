Proceso Leer10Numeros_MostrarInformacion	

    Definir sumaTotal, sumaImpares, totalPares, i Como Entero
	sumaTotal = 0 
	sumaImpares = 0 
	totalPares = 0

	i=1
	Mientras i <= 10 Hacer
        Escribir "Digita un numero: "
		leer num
		
		Si (num MOD 2) == 0 Entonces
			totalPares = totalPares + 1
		SiNo
			sumaImpares <- sumaImpares + i
		FinSi

        sumaTotal = sumaTotal + i
		i = i + 1
	FinMientras

	Escribir "La suma total es " , sumaTotal
	Escribir "Hay ", totalPares, " numeros pares"
	Escribir "La suma de los impares es " , sumaImpares
FinProceso
