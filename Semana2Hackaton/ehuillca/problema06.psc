Proceso ValidarFecha

    Dimension dias_mes[12]

    dias_mes[1] = 31
    dias_mes[3] = 31
    dias_mes[5] = 31
    dias_mes[7] = 31
    dias_mes[8] = 31
    dias_mes[10] = 31
    dias_mes[12] = 31
    
    dias_mes[2] = 28
    dias_mes[4] = 30
    dias_mes[6] = 30
    dias_mes[9] = 30
    dias_mes[11] = 30
    
	Escribir "**** VALIDAR FECHA ****"
	
	Escribir " Ingresa el dia: "
	Leer Dia
	  
	Escribir " Ingresa el mes: "
	Leer Mes
	
	Escribir " Ingresa el ANIo: "
	Leer Anio

    Si(((Anio MOD 4 = 0) & (Anio MOD 100) <> 0) | (Anio MOD 400) = 0)
        dias_mes[2] = dias_mes[2] + 1
    FinSi

    Si (Mes < 1 | Mes > 12)
        Escribir "La fecha ingresada es incorrecta, el mes no es correcto " Mes
    Sino
        Mes = Mes - 1
        Si(Dia <= 0 | Dia > dias_mes[Mes])
			Escribir "La fecha ingresada es incorrecta, el dia ingresado no es correcto" Dia
		Sino
			Escribir "La fecha " Dia "-" Mes "-" Anio " es una fecha valida"
		FinSi
    FinSi
	
FinProceso