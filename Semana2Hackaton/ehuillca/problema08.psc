Proceso NominaSalarialNeto

	Escribir "Horas trabajadas: "
    Leer TotalHoras
    
	Escribir "Pago x Hora: "
    Leer PagoHora
	
	SalarioNeto = 0

    Si (TotalHoras <= 35)
        SalarioNeto = TotalHoras * PagoHora
    SiNo
        SalarioNeto = (35 * PagoHora) + ((TotalHoras - 35) * (PagoHora + 1.5))
    FinSi
    
    Si (SalarioNeto >= 20000)
        SalarioNeto = 0.8 * SalarioNeto
    FinSi
    
    Escribir "Tu sueldo neto es " SalarioNeto	
   
FinProceso
