Proceso NPrimos
    Definir  num, valor, divisores Como Entero

	Escribir "Numeros primos menores a 100"
	
	Para num = 1 hasta 100 Hacer
        valor = 1
        divisores = 0

        Mientras valor <= num Hacer
            Si (num mod valor) == 0 Entonces
                divisores = divisores + 1
            FinSi
            valor = valor + 1
        FinMientras

        Si divisores == 2 Entonces
            Escribir "El numero ", num, " es primo"
        FinSi        		
	FinPara
	
FinProceso