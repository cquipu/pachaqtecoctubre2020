Proceso DividirMayorEntreMenor

	Numero1 = 0
	Numero2 = 0
	
	Escribir "Escriba el Primer Numero"
	Leer Numero1

	Escribir "Escriba el Segundo Numero"
	Leer Numero2
	
	Si (Numero1 > Numero2)
		Si (Numero2 <> 0)
			Resultado = Numero1 / Numero2
			Escribir "El mayor numero entre el menor numero es " Resultado
		Sino
			Escribir "No se puede dividir entre cero"
		FinSi
	Sino
		Si (Numero1 <> 0)
			Resultado = Numero2 / Numero1
			Escribir "El mayor numero entre el menor numero es " Resultado
		Sino
			Escribir "No se puede dividir entre cero"
		FinSi
	FinSi

FinProceso
