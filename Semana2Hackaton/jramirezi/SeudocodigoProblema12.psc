//Algoritmo que lea n�meros enteros hasta teclear 0, y nos muestre el m�ximo, el m�nimo y la media de todos ellos. Piensa como debemos inicializar las variables.
Proceso Leer10Numeros
	Definir n, min, max,suma,prom,i Como Entero
	Escribir "ingrese un numero (con cero sale del programa): "
	Leer n
	Si n=0 Entonces  //Para el caso que n=0
		min=0
		max=0
		prom=0
	SiNo
		i=1
		min=n
		max=n
		suma=0
		Mientras n<>0 Hacer
			
			Si n>max Entonces
				max=n
			Fin Si	
			Si n<min	Entonces
				min=n
			Fin Si
			suma=suma+n
			
			Escribir "ingrese un numero: (con cero sale del programa): "
			Leer n
			i=i+1
		Fin Mientras
		prom=suma/(i-1)
	Fin Si
	Escribir "El Maximo es: ",max
	Escribir "El Minimo es: ",min
	Escribir "El promedio es: ",prom
FinProceso
