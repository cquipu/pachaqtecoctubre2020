//Una tienda ofrece un descuento del 15% sobre el total de la compra durante el mes de octubre.
// Dado un mes y un importe, calcular cu�l es la cantidad que se debe cobrar al cliente.
Proceso DescuentoOctube
	Definir validarMes Como Logico
	Definir mes Como Caracter
	Definir monto Como Real
	Escribir "Program que hace un descuento del 15% en Octubre"
	escribir "Ingresa un mes"
	Leer mes
	Escribir "Ingresar monto: "
	Leer monto
	Dimension i[5]
	i[1]="Octubre"
	i[2]="OCTUBRE"
	i[3]="octubre"
	i[4]="oCtubre"
	i[5]="oCTUBRE"
	validarMes=Falso
	x=1
	Para x=1 Hasta 5 Con Paso 1 Hacer
		Si (i[x]==mes) Entonces
			validarMes=Verdadero
			x=5
			Fin Si
	Fin Para
	Si (validarMes) Entonces
		Escribir "Es Mes de octube y usted tiene un descuento del 15%, Ust�d s�lo paga:", monto*0.75
	SiNo
		Escribir "Usted debe pagar el monto con tarifa regular: ", monto
	Fin Si
FinProceso
