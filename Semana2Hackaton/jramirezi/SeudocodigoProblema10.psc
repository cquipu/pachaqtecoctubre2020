//Hacer un Pseudoc�digo que calcule el factorial de un n�mero.
Proceso Factorial
	Definir n, fact Como Entero
	
	Escribir "======= Programa que calcula el Facorial ========"
	Escribir ""
	Escribir "Escribir Un Numero:"
	Leer n	
	fact=n
	Si n>=1 Entonces
		Escribir "El factorial de ", n		
		Si n=1 Entonces  // Si el numero es igual a 1 su factorial es 1
			fact=1
		SiNo
			Hacer  // Sino calcular su factorial
				n=n-1
				fact=fact*n
			Hasta que n=1
		FinSi
	SiNo		// Si el numero no es un entero>=1:
		Escribir "No existe factorial:"
	Fin Si
	
	Escribir "es =", fact  //Se escribe el valor del Factorial
FinProceso
