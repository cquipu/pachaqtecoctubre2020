//Realice un Pseudoc�digo que calcule la n�mina salarial neto, de unos obreros cuyo trabajo se paga en horas. 
//El c�lculo se realiza de la siguiente forma:
//-Las primeras 35 horas a una tarifa fija.
//-Las horas extras se pagan a 1.5 m�s de la tarifa fija.
//-Las horas extras se pagan a 1.5 m�s de la tarifa fija.
//-Los impuestos a deducir de los trabajadores var�an, seg�n el sueldo mensual 
//	si el sueldo es menos a S./ 20,000.00 el sueldo es libre de impuesto 
//	y si es al contrario se cobrar� un 20% de impuesto.

Proceso Sueldo
	definir h, sueldoXhora, pagoNeto Como Entero
	Escribir "=========== Programa que calcula el Sueldo =============="
	
	Escribir "Ingrese el n�mero de horas trabajadas"
	Leer h
	Escribir "Ingresar El sueldo por Hora"
	Leer sueldoXhora
	
	Si h<=35 Entonces
		pagoNeto=h*sueldoXhora
	SiNo
		pagoNeto=sueldoXhora*35 + sueldoXhora*1.5*(h-35)
	Fin Si
	Si (pagoNeto<20000)
		Escribir "El sueldo es: ", pagoNeto
	SiNo
		Escribir "Debe Pagar Impuesto, El sueldo es: ", pagoNeto*0.8
	FinSi
FinProceso
