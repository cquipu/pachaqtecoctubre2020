

class Producto:
    # Pseudos atributos privados inician con __
    __Almacen="Bodega 1" 

    def __init__(self, Codigo, Sabor, Nombre, Precio, Presentacion):
        self.Codigo = Codigo
        self.Sabor = Sabor
        self.Nombre = Nombre
        self.Precio = Precio
        self.Presentacion = Presentacion
    
    def Vender(self):
        print("Estoy vendiendo")
    
    def Comprar(self):
        print("Estoy comprando")
    
    def Almacenar(self):
        print("Esta en el " + self.__Almacen)


class Persona:
    def __init__(self, DNI, Nombre, Apellido,  FechaNacimiento, Sexo):
        self.DNI = DNI
        self.Nombre = Nombre
        self.Apellido = Apellido
        self. FechaNacimiento = FechaNacimiento
        self.Sexo = Sexo

# Ejemplo Herencia
class Cliente(Persona):
    def __init__(self, DNI, Nombre, Apellido,  FechaNacimiento, Sexo, CodigoCliente):
        super().__init__(DNI, Nombre, Apellido,  FechaNacimiento, Sexo, CodigoCliente)
        self.CodigoCliente = CodigoCliente

objCliente = Cliente("001676345")



objProducto1 = Producto(1, "Chocolate", "Helado", 1.50, "Unidad")
objProducto2 = Producto(2, "Anis", "IncaCola", 5, "1LT")

print(objProducto1.Nombre)
objProducto1.Vender()


