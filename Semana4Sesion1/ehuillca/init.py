# Crea una clase padre llamada Vehículo, donde definas sus atributos color y precio
# Crea las clases Avión y Auto, quienes heredarán de Vehiculo
# Imprime el valor del atributo color cuando se invoque
# Finalmente, en la clase Auto, crea una variable más, de las que ya heredó de la clase anterior

class Vehiculo:
    def __init__(self, color, precio):
        self.color = color
        self.precio = precio
    
    def getPrecio(self):
        return self.precio
    
    def setPrecio(self, precio):
        self.precio = precio

    @property
    def Color(self):
        return self.color
    @Color.setter
    def Color(self, color):
        self.color = color
    
class Avion(Vehiculo):
    def __init__(self, color, precio):
        super().__init__(color, precio)

class Auto(Vehiculo):
    def __init__(self, color, precio, marca):
        super().__init__(color, precio)
        self.marca = marca

objAvion = Avion("Blanco", "9845756")
objAuto = Auto("Rojo", 12566, "Toyota")

print("Precio Base Avion: ", objAvion.getPrecio())
objAvion.setPrecio(9798752)
print("Precio Avion modificado: ", objAvion.getPrecio())


print("Marca Auto:", objAuto.marca)
print("Color Auto:", objAuto.Color)
objAuto.Color= "Rojo"
print("Nuevo Color Auto:", objAuto.Color)
