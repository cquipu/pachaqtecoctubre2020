##### Estructura de datos ###
#### Listas ####
listaEnteros = [
    1, 2, 3,
    4, 5, 6,
    7, 8, 9
]
print(listaEnteros)
listaString = ["Roberto", "David", "Pineda", "Lopez"]
listaCombinada = ["karen", 24, True, 3.14]

print(listaCombinada)
print(listaCombinada[2:])
#Agregar un nuevo elemento a la lista
listaCombinada.append("Chiclayo")
print(listaCombinada)
nuevaLista = ["Roberto", "pineda"]
listaCombinada.extend(nuevaLista)
print(listaCombinada)
listaCombinada.remove(True)
print(listaCombinada)
print(listaCombinada.index("Chiclayo"))
listaCombinada.pop(2)
print(listaCombinada)
print(len(listaCombinada))

########################################

######### Tuplas ######
print("""
    Estructura de Datos: TuPlas
""")
tuplaCombinada=(1, "karen", "Chiclayo")
print(tuplaCombinada)
print(len(tuplaCombinada))
print(tuplaCombinada.count("Karen")) #Te cuenta cuantas veces aparece "karen"
print(tuplaCombinada.index("Chiclayo"))
nuevaTupla = tuple(range(5,15)) #Crea una tupla con numeros consecutivos desde el 5 al 14
print(nuevaTupla)
terceraTupla = (1, 2, nuevaTupla)
print(terceraTupla)

######### Dicionarios ######
print("""
    Estructura de Datos: Diccionarios
""")
midiccionario = {
    "Do": 32.7,
    "Re": 36.71,
    "MI": 42.2,
    "Fa": 43.65,
    "Sol": 49,
    "La": 440,
    "Si": 61.44
}
print("Diccionario: ",midiccionario)
print(midiccionario["Fa"], "Herz")
print(type(midiccionario["Fa"]))

dicNotasMusicales = {
    "Octava1":{
        "Do": 32.7,
        "Re": 36.71,
        "MI": 42.2,
        "Fa": 43.65,
        "Sol": 49,
        "La": 440,
        "Si": 61.44
    },
    "Octava2":{
        "Do": 32.7,
        "Re": 36.71,
        "MI": 42.2,
        "Fa": 43.65,
        "Sol": 49,
        "La": 440,
        "Si": 61.44
    },
    "Octava3":{
        "Do": 32.7,
        "Re": 36.71,
        "MI": 42.2,
        "Fa": 43.65,
        "Sol": 49,
        "La": 440,
        "Si": 61.44
    },
    "Octava4":{
        "Do": 32.7,
        "Re": 36.71,
        "MI": 42.2,
        "Fa": 43.65,
        "Sol": 49,
        "La": 440,
        "Si": 61.44
    }
}
print(dicNotasMusicales)
print("")
print(dicNotasMusicales["Octava2"])
print("")
print(dicNotasMusicales["Octava2"]["Re"])
dicOctava5 = { "Octava5":
                {  
                    "Do": 32.7,
                    "Re": 36.71,
                    "Mi": 42.2,
                    "Fa": 43.65,
                    "Sol": 49,
                    "La": 550,
                    "Si": 61.44
                }
            }
dicNotasMusicales.update(dicOctava5)
print(dicNotasMusicales)