'''
Esta es una calculadora sencilla
'''
import funcionesAritmeticas

print("Bienvenido a mi calculadora")
print("Escribe la operacion que deseas realizar")
operacion = input()

print("Escogiste la operacion " + operacion)
print("Escribe el primer numero")
numero1 = int(input())
print("Escribe el segundo numero")
numero2 = int(input())

if(operacion == "Suma"):
    respuesta = funcionesAritmeticas.Suma(numero1,numero2)
    print(respuesta)
else:
    print("Operacion "+ operacion + " no implementada")


