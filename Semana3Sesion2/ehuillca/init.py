# a=3
# b=0
# print(a)
# print(b)
# a=b
# b=2

# print(a)
# print(b)

# listaValores =[1,2,3,4]
# print(listaValores)
# print(listaValores[0])

# tuplaValores=(1,2,3,4)
# print(tuplaValores[0])

personas = ["Jorge", "Juan", "Carlos", 4, 5, 6]
print(personas)
personas.append("Dexter")
print(personas)
print(personas.index("Juan"))
print(personas.index(5))
personas.remove("Carlos")
print(personas)



tuplaValores=(1,2,3,4,5)
print(5*tuplaValores)

dicionario={
    "Do": 32.7,
    "Re": 36.71,
    "Mi": 42.2,
    "Fa": 43.65,
    "Sol": 49,
    "La": 110,
    "Si": 61.44
}
print(dicionario)
print(dicionario["Do"])
print(type(dicionario))
print(type(dicionario["Re"]))