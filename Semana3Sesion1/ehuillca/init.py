# git remote add pchqtec https://gitlab.com/rpinedaec/pachaqtecoctubre2020.git
# Actualizando
# git fetch pchqtec master
# git merge pchqtec/master master

strCaracter='este es un string'
intEdad=23
blBoleano=True
n1=n2=n3=0

#Este es un comentario
print(strCaracter)

nombres='Edson Joel'
apellidos='Huillca Alarcon'
edad=31
email='ejoelhuillca@gmail.com'
telefono='912588380'

print(nombres)


########### TIPOS DE DATOS  ###########
## NUMERICOS: Entero ###########
intNumero = 12

## NUMERICOS: Float ###########
flIGV = 0.18

## NUMERICOS: Complejo ###########
cplComplejo = 1+1j


## STRING ###########
print(cplComplejo)

strNombres='Edson Joel'
strApellidos='Huillca Alarcon'

strLargo = '''Este es un texto muy largo es por eso que estamos usando un los 3 
apostrofes asi puedes excribir todo lo que desees'''

print(strLargo)

print(strLargo[5])
print(strLargo[2:6])
print(strLargo[2:])


print(strNombres + " " + strApellidos)
print(strNombres[0:5])
print(strApellidos[-5:])

## BOOLEANO ###########
blTmp=True

########### TIPOS DE DATOS  ###########
numero1=30
numero2=23
# OPERADORES ARITMETICOS
print(numero1 + numero2)
print(numero1 - numero2)
print(numero1 * numero2)
print(numero1 / numero2)
print(numero1 // numero2)
print(numero1 % numero2)
print(numero1 ** numero2)


# OPERADORES DE COMPARACION
print(numero1 == numero2)
print(numero1 != numero2)
print(numero1 <  numero2)
print(numero1 <=  numero2)
print(numero1 >  numero2)
print(numero1 >=  numero2)

bolVariable1=True
bolVariable2=False
# OPERADORES LOGICOS
print(not bolVariable1)
print(not bolVariable2)
print(bolVariable1 and bolVariable2)
print(bolVariable1 or bolVariable2)



