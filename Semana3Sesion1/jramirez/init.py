strCaracter = "Este es un String"
intEdad = 36
blBooleano = True

n1 = n2 = n3 = 0

#print(strCaracter)

nombre = "Roberto Pineada"
edad = 27
Telefono = '+51955365789' 
############## Tipos de Datos ###

#Tipo de datos Entero o integer
inNumero = 37
#Tipo float:
Igv =0.18
#tipo Long
edadDeLaTierra = 123123123
#tipo complejo
numeroComplejo = 5+3j
print(numeroComplejo)

###### String ########
strNombre = "Roberto"
strApellido = 'Pineda'
strDireccion = '''Av, Occidental
Bernardo 156'''
# print(strNombre)
# print(strApellido)
# print(strDireccion)
# print(strDireccion[5])
# print(strNombre[4])
# print(strNombre[4:6])
print(strNombre + " " + strApellido)

##### Operadores ######
###### Operadores Aritmeticos ####
# print (5 + 8) #Suma
# print (5 - 8) #Resta
# print (5 * 8) #Multiplicacion
# print (30 / 10) #division
# print (30 // 10) #division Entera
# print (6 % 4) #Modulo o el Resto
# print (2**3) #Potencia (2 al cubo)

################################
##### Operadores De Comparacion #####
num1=30
num2 = 27
# print (num1 == num2) #igualdad
# print (num1 != num2) #diferente
# print (num1 > num2) #mayor
# print (num1 < num2) #menor
# print (num1 >= num2) #mayor igual
# print (num1 <= num2) #menor igual
############################
##### Operadores Logicos ###
boolVariable1 = True
boolVariable2 = False

print(not boolVariable1)
print(not boolVariable2)

print(boolVariable1 and boolVariable2)

print(boolVariable1 or boolVariable2)