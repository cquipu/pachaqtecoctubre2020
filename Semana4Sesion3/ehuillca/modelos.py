
class Persona:
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo):
        self.DNI = DNI
        self.Nombre = Nombre
        self.Apellido = Apellido
        self.FechaNacimiento = FechaNacimiento
        self.Sexo = Sexo


class Bibliotecario(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoEmpleado):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoEmpleado = CodigoEmpleado

    def PrestarLibro(self, Libro):
        pass

    def BuscarLibro(self, Libro):
        pass

    def RecibirLibro(self, Libro):
        pass


class Alumno(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoAlumno):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoAlumno = CodigoAlumno

    def PedirPrestado(self, Libro):
        Libro.Cantidad = Libro.Cantidad - 1

    def DevolverLibro(self, Libro):
        Libro.Cantidad = Libro.Cantidad + 1


class Libro:
    def __init__(self, CodigoLibro, Titulo, Autor, Edicion, Categoria, ISBN, Editorial, Cantidad):
        self.CodigoLibro = CodigoLibro
        self.Titulo = Titulo
        self.Autor = Autor
        self.Edicion = Edicion
        self.Categoria = Categoria
        self.ISBN = ISBN
        self.Editorial = Editorial
        self.Cantidad = Cantidad


class Autor(Persona):
    def __init__(self, DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoAutor):
        super().__init__(DNI, Nombre, Apellido, FechaNacimiento, Sexo)
        self.CodigoAutor = CodigoAutor


class Editorial:
    def __init__(self, CodigoEditorial, RUC, Nombre):
        self.CodigoEditorial = CodigoEditorial
        self.RUC = RUC
        self.Nombre = Nombre


#####################################################################################
print("Bienvenido a la Biblioteca de Pachaqtec")

lstLibros = []

objAutor = Autor("1234", "Javier", "Santaolalla", "01/01/1985", "M", 1)
objEditorial = Editorial(1, "0009999999", "La Esfera De Los Libros")
objLibro = Libro(1, "El Bosón de Higgs no te va a Hacer la Cama", objAutor, "Primera",
                 "Ciencias Exactas", "9788490607725", objEditorial, 10)
lstLibros.append(objLibro)


objAutor2 = Autor("1235", "Stephen", "Hawking", "01/01/1985", "M", 1)
objEditorial2 = Editorial(1, "0008888888", "Editorial Crítica")
objLibro2 = Libro(2, "Agujeros Negros", objAutor2, "Primera",
                  "Ciencias Exactas", "9788490607725", objEditorial2, 5)
lstLibros.append(objLibro2)


def BuscarLibro(NombreLibro):
    for obj in lstLibros:
        titulo = obj.Titulo.upper()
        if titulo.find(NombreLibro.upper()) != -1:
            return obj


print(lstLibros)
for obj in lstLibros:
    print(obj.Titulo)
    print(obj.Autor.Nombre)
    print(obj.Editorial.Nombre)

salir = True
while salir:
    print("Identificate: Escribe B si eres Bibliotecario o A si eres Alumno")
    TipoUsuario = input()

    if(TipoUsuario.upper() == 'A'):
        print("Ingresa tus datos")
        DNI = input("Escribe tu DNI :")
        Nombre = input("Escribe tu Nombre :")
        Apellido = input("Escribe tu Apellido :")
        FechaNacimiento = input("Escribe tu Fecha de Nacimiento :")
        Sexo = input("Escribe tu Sexo :")
        CodigoAlumno = input("Escribe tu Codigo de Alumno:")
        objAlumno = Alumno(DNI, Nombre, Apellido,
                           FechaNacimiento, Sexo, CodigoAlumno)
        print(f"Bienvenido {objAlumno.Nombre} {objAlumno.Apellido}")
        menuAlumno = True
        while menuAlumno:
            print("Que deseas hacer: 1 - Pedir Libro o 2 - Devolver Libro o 0 - Salir")
            opcion = int(input())
            if(opcion == 1):
                print("Escribe el nombre del libro que deseas pedir")
                NombreBuscar = input()
                LibroEncontrado = BuscarLibro(NombreBuscar)
                print(LibroEncontrado.Titulo)
                objAlumno.PedirPrestado(LibroEncontrado)
                print("Listo ya has pedido el libro")
                print(LibroEncontrado.Cantidad)
            elif(opcion == 2):
                print("Escribe el nombre del libro que deseas devolver")
                NombreBuscar = input()
                LibroEncontrado = BuscarLibro(NombreBuscar)
                print(LibroEncontrado.Titulo)
                objAlumno.DevolverLibro(LibroEncontrado)
                print("Listo ya has devuelto el libro")
                print(LibroEncontrado.Cantidad)
            elif(opcion == 0):
                print("Salir")
                menuAlumno = False
            else:
                print("ingresa una opcion valida")
        salir = False
    elif(TipoUsuario.upper() == 'B'):
        print("Ingresa tus datos")
        DNI = input("Escribe tu DNI :")
        Nombre = input("Escribe tu Nombre :")
        Apellido = input("Escribe tu Apellido :")
        FechaNacimiento = input("Escribe tu Fecha de Nacimiento :")
        Sexo = input("Escribe tu Sexo :")
        CodigoEmpleado = input("Escribe tu Codigo :")
        objBibliotecario = Bibliotecario(
            DNI, Nombre, Apellido, FechaNacimiento, Sexo, CodigoEmpleado)
        print(
            f"Bienvenido {objBibliotecario.Nombre} {objBibliotecario.Apellido}")
        menuBibliotecario = True
        while menuBibliotecario:
            print("Que deseas hacer: 1 - Prestar Libro o 2 - Recibir Libro o 0 - Salir")
            opcion = int(input())
            if(opcion == 1):
                print("Prestar")
            elif(opcion == 2):
                print("Recibir")
            elif(opcion == 0):
                print("Salir")
                menuBibliotecario = False
            else:
                print("Ingresa una opcion valida")
        salir = False
    else:
        print("Opcion Incorrecta vuelve a intentarlo")
