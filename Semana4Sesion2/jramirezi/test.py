class Animal:
    def __init__(self,Tipo, Nombre):
        self.Tipo = Tipo
        self.Nombre = Nombre
    def toDicionario(self):
        self.dicionario = {}
        self.dicionario = {self}
        return self.dicionario
    def __str__(self):
        return '{},{}'.format(self.Tipo, self.Nombre)
        
gato = Animal("Felino","gato")
print(gato)
edades = {'Hector':27,'Juan':45,'Maria':34}

for edad in edades:
    print(edad)
for clave in edades:
    print(edades[clave])

days= { 'Mon', 'Tue', 'Wed','Thu'}
enum_days = enumerate(days)
print(list(enum_days))

import csv

# libros = []
# with open('dbLibros.csv') as File:
#     reader = csv.reader(File, delimiter=';', quotechar=',',
#                         quoting=csv.QUOTE_MINIMAL)
#     for row in reader:
#         print(row)
#     File.close()
#print(libros[1])

results = []
with open('dbLibros.csv') as File:
    fieldnames = ['CodigoLibro', 'Titulo', 'Autor', 'Edicion', 'Categoria', 'ISBN', 'Editorial', 'Cantidad']
    reader = csv.DictReader(File, delimiter=';')
    for row in reader:
        results.append(row)
    print (results)