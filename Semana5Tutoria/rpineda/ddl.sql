create table pais(
	idpais serial,
	nombre varchar(100),
	constraint pk_pais primary key(idpais)
);

create table tipo(
	idtipo serial,
	nombre varchar(100),
	constraint pk_tipo primary key(idtipo)
);

create table modelo(
	idmodelo serial,
	idtipo int,
	nombre varchar(100) not null,
	progreso int not null,
	isterminado bit not null,
	fechacompra date not null,
	fechaterminado date null,
	constraint pk_modelos primary key(idtipo),
  	CONSTRAINT fk_modelo_tipo
    FOREIGN KEY (idtipo)
    REFERENCES tipo (idtipo)
    ON DELETE cascade
    ON UPDATE cascade
);

alter table modelo
   add constraint UQ_modelo
   unique (idmodelo);


alter table pais
    add constraint UQ_pais
   unique (idpais);


create table fabricacion(
	idpais int not null,
	idmodelo int not null,
	fechafabricacion date null,
	constraint pk_fabricacion primary key(idpais, idmodelo),
    CONSTRAINT fk_fabricacion_pais
    FOREIGN KEY (idpais)
    REFERENCES pais (idpais)
    ON DELETE cascade
    ON UPDATE cascade,
  	CONSTRAINT fk_fabricacion_modelo
    FOREIGN KEY (idmodelo)
    REFERENCES modelo (idmodelo)
    ON DELETE cascade
    ON UPDATE cascade);
