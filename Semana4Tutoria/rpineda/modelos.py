class Persona:
    __estado = True

    def __init__(self, nombre, apellido, edad, dni):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad
        self.dni = dni
       

class Cliente(Persona):
    def __init__(self, codCliente, nombre, apellido, edad,  dni):
        super().__init__(nombre, apellido, edad,  dni)
        self.codCliente = codCliente


    def dictCliente(self):
        d = {
            'codCliente': self.codCliente,
            'nombre': self.nombre,
            'apellido': self.apellido,
            'edad': self.edad,
            'dni': self.dni,
            'married' : None
        }
        return d

class Empleado(Persona):
    def __init__(self,  nombre, apellido, edad, dni, codEmpleado):
        super().__init__(nombre, apellido, edad,  dni)
        self.codEmpleado = codEmpleado

    def marcarIngreso(self):
        print("El empleado esta marcando su ingreso")
        print("El empleado marcó su ingreso")
        
    def dictEmpleado(self):
        d = {
            'codEmpleado': self.codEmpleado,
            'nombre': self.nombre,
            'apellido': self.apellido,
            'edad': self.edad,
            'dni': self.dni
        }
        return d


class Producto:

    def __init__(self, codProducto, nombreProducto, cantidadProducto, costoProducto,TotalXProducto, TotalProductos):
        self.codProducto = codProducto
        self.nombreProducto = nombreProducto
        self.cantidadProducto = cantidadProducto
        self.costoProducto = costoProducto
        self.TotalXProducto = TotalXProducto
        self.TotalProductos = TotalProductos
        
    def __str__(self):
        return """Codigo: {} \nNombre: {}""".format(self.codProducto, self.nombreProducto)

    def dictProducto(self):
        d = {
            'codProducto': self.codProducto,
            'nombreProducto': self.nombreProducto,
            'cantidadProducto': self.cantidadProducto,
            'costoProducto': self.costoProducto,
            'TotalXProducto': self.TotalXProducto,
            'TotalProductos': self.TotalProductos
        }
        return d

    def costearProducto(self):
        print("Costeando producto")
        print("Producto costeado")

