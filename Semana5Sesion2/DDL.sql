create database pachaqtec;
use pachaqtec;

create table pachaqtecTable(
idTable int not null,
descripcion varchar(1000) not null,
constraint pk primary key(idTable)
);

create table actor(
idActor int not null,
nombreActor varchar(1000) not null,
constraint pkActor primary key(idActor)
);


alter table pachaqtecTable
add column fecha datetime;

drop table pachaqtecTable;

truncate pachaqtecTable; 

RENAME table pachaqtecTable to pachaqtecTabla;
